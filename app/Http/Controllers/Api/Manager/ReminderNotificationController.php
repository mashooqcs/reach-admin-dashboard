<?php

namespace App\Http\Controllers\Api\Manager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReminderNotification;
use DB;
use Validator;
use Hash;
class ReminderNotificationController extends Controller
{
    public function addReminder(Request $request){
        // dd('a');
        $validator = Validator::make($request->all(), [      
            'title'         => 'required',
            'start_date'    => 'required|date_format:Y-m-d',
            'start_time'    => 'required|date_format:H:i',
            'repeat_on'     => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $user = Auth()->user();
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['title']         = $request->input('title');
        $dataToAdd['user_id']       = $user->id;
        $dataToAdd['start_date']    = $request->input('start_date');
        $dataToAdd['start_time']    = $request->input('start_time');
        $dataToAdd['repeat_on']     = $request->input('repeat_on');
        $dataToAdd['created_at']    = $currentDateAndTime;
        $dataToAdd['updated_at']    = $currentDateAndTime;

        $add = ReminderNotification::create($dataToAdd);
        return response()->json(['message' => 'Reminder added successfully.'], 200);
    }
}
