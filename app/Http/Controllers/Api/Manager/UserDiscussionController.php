<?php

namespace App\Http\Controllers\Api\Manager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserDiscussion;
use App\Models\User;
use DB;
use Validator;


class UserDiscussionController extends Controller
{
    public function discussionBoard(Request $request){
        $limit      = $request->query('limit')??10;
        $offset     = $request->query('offset')??0;
        $authUser = Auth()->user();
        $chat = UserDiscussion::where('sender_id', $authUser->id)->orWhere('recipient_id', $authUser->id);
        $count = count($chat->get());

        $chat = $chat->limit($limit)->offset($offset)->get();
        return response()->json(array('data' => $chat, 'count'=> $count), 200);

    }
    public function addDiscussion(Request $request){
        $validator = Validator::make($request->all(), [      
            'message'  => 'required',
            'user_id'  => 'required',
            

        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $authUser = Auth()->user();
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['message'] = $request->input('message');
        $dataToAdd['sender_id'] = $authUser->id;
        $dataToAdd['recipient_id'] = $request->input('user_id');
        $dataToAdd['sender_type'] = 'manager';
        $dataToAdd['recipient_type'] = 'agent';
        $userDiscussionModel = new UserDiscussion;
        $add = $userDiscussionModel->store($dataToAdd);
        if($add){
            return response()->json(['message' => 'Message sent.'], 200);
        }
        else{
            return response()->json(['error' => 'Error sending message'], 400);

        }
    }
    
}
