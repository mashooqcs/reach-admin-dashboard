<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Validator;
use DB;
use Hash;
use Carbon;
use App\Models\User;
use App\Models\Role;
use App\Models\Token;
use App\Models\UserSubscription;
use App\Models\Feedback;


class UserController extends Controller
{
    //define constants here
    const USER_STATUS_INACTIVE          = "0";
    const USER_STATUS_ACTIVE            = "1";
    const USER_STATUS_BLOCKED           = "2";
    const USER_STATUS_DELETED           = "3";
    const USER_ROLE_ADMIN               = "1";
    const USER_ROLE_MANAGER             = "2";
    const USER_ROLE_AGENT               = "3";




    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required',
            'password'  => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
            //return response()->json(['status' => '400', 'error' => array('message' => $validator->errors()->first())], 400);
        }
        $input = $request->all();
        $userEmail = $input['email'];

        $userModel = new User;
        $checkWithEmail = $userModel->findUserByEmail($input['email'], self::USER_ROLE_MANAGER);
        if (!$checkWithEmail) {
            return response()->json(['message' => 'User does not exist with this email address.'], 400);
        }
        

        if (Auth::attempt(['email' => $userEmail, 'password' => request('password')])) {
            $user       = Auth::user();
            $accessTokenModel = new Token;
            $destroySessions  = $accessTokenModel->destroySessions($user->id);
            $token      = $user->createToken('REACH')->accessToken;
            $userStatus = $user->status;
            $userId     = $user->id;
            if ($userStatus == self::USER_STATUS_INACTIVE) {
                return response()->json(['message' => 'User status unverified.'], 400);
            }
            if ($userStatus == self::USER_STATUS_BLOCKED) {
                return response()->json(['message' => 'This account has been banned.'], 400);
            }

            //update user info
            $userUpdate         = array();
            $deviceUpdateStatus = false;

            if (isset($input['device_id'])) {
                $userUpdate['device_id'] = $input['device_id'];
                $deviceUpdateStatus = true;
            }
            if (isset($input['device_type'])) {
                $userUpdate['device_type'] = $input['device_type'];
                $deviceUpdateStatus = true;
            }
            if ($deviceUpdateStatus) {
                $updateUserInfo = $userModel->updateUser($userId, $userUpdate);
            }

            
            if($user->profile_image){
                $userImage  = asset("/assets/manager/images");
                $userImage  = $userImage."/".$user->profile_image;
            }
            else{
                $userImage  = asset("/assets/manager/images")."/img-placeholder.png";
            }
            
            $subscriptionModel  = new UserSubscription;
            $userSubscription   = $subscriptionModel->getUserSubscription($userId);
            $regigstrationFee = null;
            $paymentStatus = 'unpaid';
            if($userSubscription){
               $regigstrationFee = $userSubscription->registration_fee;
               $paymentStatus = 'paid';
            }

            //user info
            $success = array(
                'message'           => 'Login successfull',
                'payment_status'    => $paymentStatus,
                'fee_enabled'       => $user->fee_enabled?true:false,
                'registration_fee'  => $regigstrationFee,
                'access_token'      => $token,
                'user_info'         => array('personal_info' => array('user_id' => $userId,
                                                                    'first_name'=> $user->first_name, 
                                                                    'last_name'=> $user->last_name,
                                                                    'number'=> $user->phone_number,
                                                                    'email'=> $user->email,
                                                                    'image'=> $userImage

                                                                 ),
                                            'address_info' => array('address'=> $user->address, 
                                                                    'country'=> $user->country,
                                                                    'state'=> $user->state,
                                                                    'city'=> $user->city,
                                                                    'zip_code'=> $user->zip_code
                                                                ),
                                            'rejection_reason'  => $user->rejection_reason,
                                            'manager_id'        => $user->manager_id,
                                            'registration_date' => $user->created_at,                     
                                            )
            );


            return response()->json($success, 200);
        } else {
            return response()->json(['message' => 'Login failed, please check your credentials and try again.'], 400);
        }
    }

    public function contactUs(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => 'required|email',
            'subject'           => 'required',
            'message'           => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(),], 400);
        }
        $user       = Auth::user();
        $input = $request->all();
        $feedbackModel = new Feedback;
        $dataToAdd['user_id'] = $user->id;
        $dataToAdd['first_name'] = $input['first_name'];
        $dataToAdd['last_name'] = $input['last_name'];
        $dataToAdd['email'] = $input['email'];
        $dataToAdd['subject'] = $input['subject'];
        $dataToAdd['message'] = $input['message'];
        $addFeedback   = $feedbackModel->addFeedback($dataToAdd);
        if($addFeedback){
            return response()->json(['message' => 'Your message has been sent'], 201);
        }
        return response()->json(['message' => 'Error adding feedback'], 400);

        
    }

    //reset password request
    public function resetPasswordRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        
        $input          = $request->all();
        $userEmail      = $input['email'];

        $userModel          = new User;
        $user     = $userModel->findUserByEmail($userEmail, self::USER_ROLE_MANAGER);

        if (!$user){
            return response()->json(['message' => 'User does not exists.'], 400);
        }
        if ($user->status == self::USER_STATUS_INACTIVE) {
            return response()->json(['message' => 'User status unverified.'], 400);
        }
        if ($user->status == self::USER_STATUS_BLOCKED) {
            return response()->json(['message' => 'This account has been banned.'], 400);
        }
        if ($user->status == self::USER_STATUS_DELETED) {
            return response()->json(['message' => 'Account does not exists.'], 400);
        }

        $digits = 4;
        $verificationCode = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        //email to user for code
        Mail::raw("REACH password reset code is: $verificationCode", function ($message) use ($userEmail) {
            $message->to($userEmail)
                ->subject('Password reset Code - REACH')->from('salcogen2021@gmail.com');
        });

        $startDate                      = time();
        $resetExpiry                    = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));
        $dataToUpdate['verification_code_expiry']= $resetExpiry;
        $dataToUpdate['verification_code']       = $verificationCode;
        $updateUserStatus     = $userModel->updateUser($user->id, $dataToUpdate);
        //update user veriy code
        if ($updateUserStatus) {
            return response()->json(['message' => 'Email sent, please check your email for security code'], 200);
        }
        return response()->json(['message' => 'Error sending code.'], 400);

    }

    public function verifyCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email'             => ['required', 'email', 'regex:^([0-9a-zA-Z]+[.-_]{0,1})+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,}$^'],
            'code'              => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }


        $input              = $request->all();
        $userEmail          = $input['email'];
        $userGivenCode      = $input['code'];
        $userModel          = new User;
        $userStoredCode     = $userModel->findUserByEmail($userEmail, self::USER_ROLE_MANAGER);
        if (!$userStoredCode) {
            return response()->json(['message' => 'User does not exist with this email address.'], 400);
        }

        if ($userStoredCode->status == self::USER_STATUS_BLOCKED) {
            return response()->json(['message' => 'User status blocked.'], 400);
        }
        if ($userGivenCode == $userStoredCode->verification_code) {
            $dataToUpdate['status']         = self::USER_STATUS_ACTIVE;
            $dataToUpdate['verification_code_expiry']   = "";
            $dataToUpdate['verification_code']    = "";

            $updateUserStatus     = $userModel->updateUser($userStoredCode->id, $dataToUpdate);

            return response()->json(['message' => 'Code accepted.'], 200);

        } else {
            return response()->json(['message' => 'The code you have entered is invalid.'], 400);
        }
    }

    //reset password
    public function updatePassword(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [

            'email'     => ['required', 'email', 'regex:^([0-9a-zA-Z]+[.-_]{0,1})+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,}$^'],
            'password' => ['required','min:6', 'max:16'],

        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $email      = $input['email'];
        $password   = $input['password'];
        //if user does not exit with given email
        $userModel      = new User;
        $checkUser      = $userModel->findUserByEmail($email, self::USER_ROLE_MANAGER);
        if (!$checkUser) {
            return response()->json(['message' => 'User does not exist with this email address.'], 400);
        }

        //update user password
        $password                       = bcrypt($input['password']);
        $dataToUpdate['password']       =  $password;
        $updateUserPassword     = $userModel->updateUser($checkUser->id, $dataToUpdate);

        if ($updateUserPassword) {
            return response()->json(['message' => 'Code accepted.'], 200);
            
        } else {
            return response()->json(['message' => 'Error updating password.'], 400);
        }
    }

    public function updateProfile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name'        => 'required|min:3|max:12|regex:/^[a-zA-Z0-9\s]+$/',
            'last_name'         => 'required|min:3|max:12|regex:/^[a-zA-Z0-9\s]+$/',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        
        $input = $request->all();
        $user       = Auth::user();
        $userId     = $user->id;
        $userModel = new User;
        //userinfo
        
        $dataToUpdate['first_name'] = $input['first_name'];
        $dataToUpdate['last_name']  = $input['last_name'];
    
        if (isset($input['number'])) {
            $dataToUpdate['phone_number'] = $input['number'];
        }
        else{
            $checkNull =  array_key_exists('number', $input);
            if ($checkNull) {
                $dataToUpdate['phone_number'] = "";
            }
        }

        if (isset($input['address'])) {
            $dataToUpdate['address'] = $input['address'];
        }
        else{
            $checkNull =  array_key_exists('address', $input);
            if ($checkNull) {
                $dataToUpdate['address'] = "";
            }
        }

        if (isset($input['country'])) {
            $dataToUpdate['country'] = $input['country'];
        }
        else{
            $checkNull =  array_key_exists('country', $input);
            if ($checkNull) {
                $dataToUpdate['country'] = "";
            }
        }

        if (isset($input['state'])) {
            $dataToUpdate['state'] = $input['state'];
        }
        else{
            $checkNull =  array_key_exists('state', $input);
            if ($checkNull) {
                $dataToUpdate['state'] = "";
            }
        }

        if (isset($input['city'])) {
            $dataToUpdate['city'] = $input['city'];
        }
        else{
            $checkNull =  array_key_exists('city', $input);
            if ($checkNull) {
                $dataToUpdate['city'] = "";
            }
        }
        if (isset($input['zip_code'])) {
            $dataToUpdate['zip_code'] = $input['zip_code'];
        }
        else{
            $checkNull =  array_key_exists('zip_code', $input);
            if ($checkNull) {
                $dataToUpdate['zip_code'] = "";
            }
        }

        //userprofile image
        $validator = Validator::make($request->all(), [
            'image'       => 'image|mimes:jpeg,png,jpg|max:5048',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/manager/images");
            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToUpdate['profile_image'] = $fullImagePath;

        }

        $updateUserStatus     = $userModel->updateUser($user->id, $dataToUpdate);
        $user = $userModel->findUserByEmail($user->email, self::USER_ROLE_MANAGER);

        if($user->profile_image){
            $userImage  = asset("/assets/manager/images");
            $userImage  = $userImage."/".$user->profile_image;
        }
        else{
            $userImage  = asset("/assets/manager/images")."/img-placeholder.png";
        }
        $subscriptionModel  = new UserSubscription;
        $userSubscription   = $subscriptionModel->getUserSubscription($userId);
        $regigstrationFee = null;
        $paymentStatus = 'unpaid';
        if($userSubscription){
           $regigstrationFee = $userSubscription->registration_fee;
           $paymentStatus = 'paid';
        }

        //user info
        $success = array(
            'message'           => 'Profile updated',
            'payment_status'    => $paymentStatus,
            'fee_enabled'       => $user->fee_enabled?true:false,
            'registration_fee'  => $regigstrationFee,
            'user_info'         => array('personal_info' => array('user_id' => $userId,
                                                                'first_name'=> $user->first_name, 
                                                                'last_name'=> $user->last_name,
                                                                'number'=> $user->phone_number,
                                                                'email'=> $user->email,
                                                                'image'=> $userImage

                                                             ),
                                        'address_info' => array('address'=> $user->address, 
                                                                'country'=> $user->country,
                                                                'state'=> $user->state,
                                                                'city'=> $user->city,
                                                                'zip_code'=> $user->zip_code
                                                            ),
                                        'rejection_reason'  => $user->rejection_reason,
                                        'manager_id'        => $user->manager_id,
                                        'registration_date' => $user->created_at,                     
                                        )
        );


        return response()->json($success, 200);

        return response()->json(['message' => 'Profile updated.'], 200);
    }



    public function getMyProfile(Request $request)
    {
      
            $user = Auth::user();
            $userId = $user->id;
            if($user->profile_image){
                $userImage  = asset("/assets/manager/images");
                $userImage  = $userImage."/".$user->profile_image;
            }
            else{
                $userImage  = asset("/assets/manager/images")."/img-placeholder.png";
            }
            $subscriptionModel  = new UserSubscription;
             $userSubscription   = $subscriptionModel->getUserSubscription($userId);
            $regigstrationFee = null;
            $paymentStatus = 'unpaid';
            if($userSubscription){
               $regigstrationFee = $userSubscription->registration_fee;
               $paymentStatus = 'paid';
            }

            //user info
            $success = array(
                'payment_status'    => $paymentStatus,
                'fee_enabled'       => $user->fee_enabled?true:false,
                'registration_fee'  => $regigstrationFee,
                'user_info'         => array('personal_info' => array('user_id' => $userId,
                                                                    'first_name'=> $user->first_name, 
                                                                    'last_name'=> $user->last_name,
                                                                    'number'=> $user->phone_number,
                                                                    'email'=> $user->email,
                                                                    'image'=> $userImage

                                                                 ),
                                            'address_info' => array('address'=> $user->address, 
                                                                    'country'=> $user->country,
                                                                    'state'=> $user->state,
                                                                    'city'=> $user->city,
                                                                    'zip_code'=> $user->zip_code
                                                                ),
                                            'rejection_reason'  => $user->rejection_reason,
                                            'manager_id'        => $user->manager_id,
                                            'registration_date' => $user->created_at,                     
                                        )
            );

            return response()->json($success, 200);

    }
    
    //change password
    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [      
            'new_password' => ['required','min:6', 'max:16'],
            'current_password'  => ['required'],

        ]);
		if ($validator->fails()) { 
		    return response()->json(['message' => $validator->errors()->first()], 400);            
        }
        
        $input              = $request->all();
        $oldPassword        = $input['current_password'];
        $newPassword        = $input['new_password'];
        $user               = Auth::user();
        $userId             = $user->id;
        $email              = $user->email;
        $userModel          = new User;
        
        if($oldPassword == $newPassword){
            return response()->json(['message' => 'New password cannot be same as current password.'], 400);            
        }
        $userOldPassword    = $user->password;
        $passwordIsSame     = Hash::check($oldPassword, $userOldPassword);      
        if(!$passwordIsSame){
            return response()->json(['message' => 'Current password is incorrect.'], 400);            
        }
        
        //update user password
        $password           = bcrypt($newPassword);
        $userUpdate['password']    =   $password;

        $updateUserInfo = $userModel->updateUser($userId, $userUpdate);

        if($updateUserInfo){
            return response()->json(['message' => 'Password updated successfully.'], 200);
        }
        else{
            return response()->json(['message' => 'Error updating password.'], 400);            
        }

    }
    public function payRegistration(Request $request){
        $validator = Validator::make($request->all(), [
            'registration_fee'      => 'required',
            'card_holder_name'      => 'required',
            'card_number'           => 'required',
            'expiry_month'          => 'required|digits:2|between:1,12',
            'expiry_year'           => 'required|date_format:Y|after:today',
            'cvv'                   => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $user       = Auth::user();
        $input = $request->all();
        $subscriptionModel = new UserSubscription;
        $dataToAdd['user_id'] = $user->id;
        $dataToAdd['registration_fee']  = $input['registration_fee'];
        $dataToAdd['card_holder_name']  = $input['card_holder_name'];;
        $dataToAdd['card_number']       = $input['card_number'];;
        $dataToAdd['expiry_month']      = $input['expiry_month'];;
        $dataToAdd['expiry_year']       = $input['expiry_year'];;
        $dataToAdd['cvv']               = $input['cvv'];;
        $addPayment = $subscriptionModel->addSubscription($user->id, $dataToAdd);
        if($addPayment){
            return response()->json(['message' => 'Payment added successfully.'], 201);            
        }
        return response()->json(['message' => 'Error adding payment.'], 400);         


    }
    public function getAgents(Request $request){

        $validator = Validator::make($request->all(), [
            'limit'     => 'required',
            'offset'    => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
       
        $fromDate   = $request->query('from_date');
        $toDate     = $request->query('to_date');
        $keyword    = $request->query('keyword');
        $limit      = $request->query('limit')??10;
        $offset     = $request->query('offset')??0;

        $userModel  = new User;
        $getAgents  = $userModel->getAgents($fromDate, $toDate, $keyword, $limit, $offset);
        
        return response()->json($getAgents, 200);
        
    }
    public function removeAgent($userId){
        $userModel = new User;
        $checkUser = $userModel->findUserById($userId);
        if((!$checkUser )|| ($checkUser->role_id != self::USER_ROLE_AGENT)){
            return response()->json(['message' => 'Invalid agent id.'], 400);            
        }
        $dataToUpdate['status'] = self::USER_STATUS_DELETED;
        $removeAgent = $userModel->updateUser($userId, $dataToUpdate);
        if($removeAgent){
            return response()->json(['message' => 'Agent removed successfully.'], 200);            
        }
        return response()->json(['message' => 'Error removing agent.'], 400);            

    }

    public function agentDetails($agentId){
        $userModel = new User;
        $user = $userModel->findUserById($agentId);
        if((!$user )|| ($user->role_id != self::USER_ROLE_AGENT)){
            return response()->json(['message' => 'Invalid agent id.'], 400);            
        }
        $userId = $user->id;
        if($user->profile_image){
            $userImage  = asset("/assets/agent/images");
            $userImage  = $userImage."/".$user->profile_image;
        }
        else{
            $userImage  = asset("/assets/agent/images")."/img-placeholder.png";
        }
        
        $subscriptionModel  = new UserSubscription;
        $userSubscription   = $subscriptionModel->getUserSubscription($userId);
        $regigstrationFee = null;
        $paymentStatus = 'unpaid';
        if($userSubscription){
           $regigstrationFee = $userSubscription->registration_fee;
           $paymentStatus = 'paid';
        }

        //user info
        $success = array(
            'payment_status'    => $paymentStatus,
            'fee_enabled'       => $user->fee_enabled?true:false,
            'registration_fee'  => $regigstrationFee,
            'user_info'         => array('personal_info' => array('user_id' => $userId,
                                                                'first_name'=> $user->first_name, 
                                                                'last_name'=> $user->last_name,
                                                                'number'=> $user->phone_number,
                                                                'email'=> $user->email,
                                                                'image'=> $userImage

                                                             ),
                                        'address_info' => array('address'=> $user->address, 
                                                                'country'=> $user->country,
                                                                'state'=> $user->state,
                                                                'city'=> $user->city,
                                                                'zip_code'=> $user->zip_code
                                                            ),
                                        'rejection_reason'  => $user->rejection_reason,
                                        'manager_id'        => $user->manager_id,
                                        'registration_date' => $user->created_at,                     
                                        )
        );


        return response()->json($success, 200);
       
    }

    public function logout()
    {

        $user       = Auth::user();
        $userId     = $user->id;
        $userModel = new User;
        $accessTokenModel = new Token;
        $destroySessions  = $accessTokenModel->destroySessions($userId);

        //remove device token so that user fcm push notifications does not work
        $dataToUpdate['device_id']   =  "";
        $updateUserPassword     = $userModel->updateUser($userId, $dataToUpdate);
        return response()->json(['message' => 'logged out successfully.'], 200);
    }

   
}