<?php

namespace App\Http\Controllers\Api\Manager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Category;
use DB;


class NotificationController extends Controller
{
    public function getMyNotifications(Request $request){
        $to         = $request->query('to');
        $from       = $request->query('from');
        $limit      = $request->query('limit')??10;
        $offset     = $request->query('offset')??0;
        $authUser   = Auth()->user();
        
        $not = Notification::where('user_id', $authUser->id);
        if($to){
            $not->where('notifications.created_at', '>=', $to);
        }
        if($from){
            $not->where('notifications.created_at', '<=', $from);
        }
        $count = count($not->get());
        $not = $not->limit($limit)->offset($offset)->orderBy('notifications.id', 'DESC')->get();


        return response()->json(array('data' => $not, 'count'=> $count), 200);;
    }
}
