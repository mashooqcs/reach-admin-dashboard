<?php

namespace App\Http\Controllers\Api\Manager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Category;
use App\Models\UserActivity;
use App\Models\Activity;
use App\Models\User;
use Validator;
use DB;


class ActivityController extends Controller
{
    public function assignAgentActivity(Request $request){
        $validator = Validator::make($request->all(), [
            'agent_id'     => 'required',
            'activity_id'  => 'required',
            'goal'         => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $userModel = new User;
        $checkUser = $userModel->findUserById($userId);
        if((!$checkUser )|| ($checkUser->role_id != self::USER_ROLE_AGENT)){
            return response()->json(['message' => 'Invalid agent id'], 400);            
        }
        $input                      = $request->all();
        $userActivityModel          = new UserActivity;
        $dataToAdd['user_id']       = $input['agent_id'];
        $dataToAdd['activity_id']   = $input['activity_id'];
        $dataToAdd['status']        = '0';
        $dataToAdd['goal']          = $input['goal'];
        $add = $userActivityModel->assignAgentActivity($dataToAdd);
        if($add){
            return response()->json(['message' => 'Activity assigned successfully.'], 200);
        }
        return response()->json(['message' => 'Error assigning activity.'], 400);
    }
    public function getAllActivitiesByCategory($categoryId, Request $request){
        $limit      = $request->query('limit')??10;
        $offset     = $request->query('offset')??0;
        $activityModel = new Activity;
        $acts = $activityModel->findActivitiesByCategory($categoryId, $limit, $offset);
        return response()->json($acts, 200);

    }
    public function getAgentActivities(Request $request){
        $validator = Validator::make($request->all(), [
            'agent_id'     => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $limit      = $request->query('limit')??10;
        $offset     = $request->query('offset')??0;
        $agentId    = $request->query('agent_id');
        $hourly     = $request->query('hourly');
        $numeric    = $request->query('numeric');
        $fromDate   = $request->query('from_date');
        $toDate     = $request->query('to_date');
        $keyword    = $request->query('keyword');
        $activityModel = new Activity;
        $acts = $activityModel->getAgentActivities($agentId, $limit, $offset, $hourly, $numeric, $fromDate, $toDate, $keyword);
        return response()->json($acts, 200);
    }
    
}
