<?php

namespace App\Http\Controllers\Api\Manager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Models\PaymentLog;
use Validator;
use DB;

class PaymentLogController extends Controller
{
   public function paymentLogs(Request $request){
       $limit  = $request->query('limit')??10;
       $offset = $request->query('offset')??0;
       $user   = Auth::user();
       $paymentLogModel = new PaymentLog;
       $getLogs = $paymentLogModel->getPaymentLogs($user->id, $limit, $offset);
       return response()->json($getLogs, 200);

   }
}
