<?php

namespace App\Http\Controllers\Api\Manager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    public function index(Request $request){
        $limit      = $request->query('limit')??10;
        $offset     = $request->query('offset')??0;
        $categoryModel = new Category;
        $cats = $categoryModel->getAllCategories($limit, $offset);
        return response()->json($cats, 200);

    }
   
    
}
