<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feedback;
use DB;
class FeedbackController extends Controller
{
    public function index(){
        $agentFeedback = Feedback::leftJoin('users', 'users.id', '=', 'feedback.user_id')->leftJoin('roles', 'roles.id', '=', 'feedback.user_id')
        ->select('feedback.*')->addSelect('users.*')->addSelect('roles.*')->addSelect('feedback.id as feedback_id')->addSelect('feedback.created_at as created_at')
        ->where('roles.name', 'agent')
        ->orderBy('feedback.id', 'DESC')->get();
        $managerFeedback = Feedback::leftJoin('users', 'users.id', '=', 'feedback.user_id')->leftJoin('roles', 'roles.id', '=', 'feedback.user_id')
        ->select('feedback.*')->addSelect('users.*')->addSelect('roles.*')->addSelect('feedback.id as feedback_id')->addSelect('feedback.created_at as created_at')
        ->where('roles.name', 'manager')
        ->orderBy('feedback.id', 'DESC')->get();

        $data = array('agentFeedback' => $agentFeedback, 'managerFeedback'=> $managerFeedback);

        return view('admin.feedback', ['data' => $data]);
    }
    public function feedbackDetailsManager($id){

        $agentFeedback = Feedback::leftJoin('users', 'users.id', '=', 'feedback.user_id')->leftJoin('roles', 'roles.id', '=', 'feedback.user_id')
        ->where('feedback.id', $id)
        ->select('feedback.*')->addSelect('users.*')->addSelect('roles.*')->addSelect('feedback.id as feedback_id')->addSelect('feedback.created_at as created_at')
        ->get();
        return view('admin.feedback-details-manager', ['data'=> $agentFeedback]);
    }
    public function feedbackDetailsAgent($id){

        $agentFeedback = Feedback::leftJoin('users', 'users.id', '=', 'feedback.user_id')->leftJoin('roles', 'roles.id', '=', 'feedback.user_id')
        ->where('feedback.id', $id)
        ->select('feedback.*')->addSelect('users.*')->addSelect('roles.*')->addSelect('feedback.id as feedback_id')->addSelect('feedback.created_at as created_at')
        ->get();
        return view('admin.feedback-details-agent', ['data'=> $agentFeedback]);
    }
    public function deleteFeedback($id){
        Feedback::where('id', $id)->delete();
        return redirect()->back();
    }
}
