<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ActualPayment;

class SettingController extends Controller
{
    public function index(){
        $data = ActualPayment::first();
        return view('admin.settings', ['data' => $data]);
    }
    public function editSettings(){
        $data = ActualPayment::first();
        return view('admin.edit-settings', ['data' => $data]);
    }
    public function updateSettings(Request $request){
       if(!$request->input('agent_fee')){
           $agentFee = 0;
       }
       else{
           $agentFee = $request->input('agent_fee');

       }
       if(!$request->input('manager_fee')){
            $managerFee = 0;
        }
        else{
            $managerFee = $request->input('manager_fee');
        }
        $payment = ActualPayment::first();
        if(!$payment){
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['agent_fee']     = $agentFee;
            $dataToAdd['manager_fee']     = $managerFee;
            $dataToAdd['status']        = '1';
            $dataToAdd['created_at']    = $currentDateAndTime;
            $dataToAdd['update_at']     =  $currentDateAndTime;
            $add = ActualPayment::create($dataToAdd);
        }
        else{
            $update = ActualPayment::where('id', $payment->id)->update(['agent_fee'=>$agentFee, 'manager_fee' => $managerFee]);
        }
        return redirect()->back()->with('success', 'Settings updated successfully.');


    }
}
