<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use DB;
use Validator;
use Hash;
use Mail;

class UserController extends Controller
{

    public function agents(Request $request){
        $user = auth()->user();
        
        $userModel  = new User;
          $managersAgents = $userModel::join('roles', 'users.role_id', '=', 'roles.id')
        ->leftJoin('payment_logs', 'users.id', '=', 'payment_logs.user_id')
        ->where('manager_id', '!=', $user->id)
        ->where('roles.name',  'agent')
        ->whereNotIn('users.status', ['0', '3'])->select('users.*')->addSelect('roles.*')->addSelect('payment_logs.*')->addSelect('users.id as user_id')->addSelect('users.created_at as created_at')->get();
        
        $myAgents = $userModel::join('roles', 'users.role_id', '=', 'roles.id')
        ->leftJoin('payment_logs', 'users.id', '=', 'payment_logs.user_id')
        ->where('manager_id', $user->id)
        ->where('roles.name',  'agent')
        ->whereNotIn('users.status', ['0', '3'])->select('users.*')->addSelect('roles.*')->addSelect('payment_logs.*')->addSelect('users.id as user_id')->addSelect('users.created_at as created_at')->get();
        
        $cats = Category::where('status', '1')->get();
        $data['managersAgent'] = $managersAgents;
        $data['myAgents'] = $myAgents;
        $data['cats'] = $cats;
      
        return view('admin.agents', ['data' => $data]);
    }

    public function inActiveAgents(Request $request){
       $from    = $request->query('from');
       $to      = $request->query('to');
       $type    = $request->query('type');

        $user = auth()->user();
        $userModel  = new User;

        $managersAgents = $userModel::join('roles', 'users.role_id', '=', 'roles.id')
        ->leftJoin('payment_logs', 'users.id', '=', 'payment_logs.user_id')
        ->where('manager_id', '!=', $user->id)
        ->where('roles.name',  'agent')
        ->where('users.status', '0')->select('users.*')->addSelect('roles.*')->addSelect('payment_logs.*')->addSelect('users.id as user_id', 'users.created_at as created_at')
        ->where(function($query) use($type, $to, $from){
            if($from){
                $date = explode('/',$from);
                $month = $date[0];
                $day = $date[1];
                $year = $date[2];
                $from = $year."-".$month."-".$day;
                $query->where('users.created_at', '>=', $from);
            }if($to){
                $date = explode('/',$to);
                $month = $date[0];
                $day = $date[1];
                $year = $date[2];
                $to = $year."-".$month."-".$day;
                $query->where('users.created_at', '<=', $to);
            }
        })
        ->get();
        
        $myAgents = $userModel::join('roles', 'users.role_id', '=', 'roles.id')
        ->leftJoin('payment_logs', 'users.id', '=', 'payment_logs.user_id')
        ->where('manager_id', $user->id)
        ->where('roles.name',  'agent')
        ->where('users.status', '0')->select('users.*')->addSelect('roles.*')->addSelect('payment_logs.*')->addSelect('users.id as user_id', 'users.created_at as created_at')
        ->where(function($query) use($type, $to, $from){
            if($from && $type){
                $date = explode('/',$from);
                $month = $date[0];
                $day = $date[1];
                $year = $date[2];
                $from = $year."-".$month."-".$day;
                $query->where('users.created_at', '>=', $from);
            }if($to && $type){
                $date = explode('/',$to);
                $month = $date[0];
                $day = $date[1];
                $year = $date[2];
                $to = $year."-".$month."-".$day;
                $query->where('users.created_at', '<=', $to);
            }
        })
        ->get();
        
        $cats = Category::where('status', '1')->get();
        $data['managersAgent'] = $managersAgents;
        $data['myAgents'] = $myAgents;
        $data['cats'] = $cats;
        return view('admin.inactive-agents', ['data' => $data]);
    }
    public function viewRegisterAgent(){
        $user = Auth()->user();
        $managers = User::where('role_id', '2')->where('manager_id', '!=', $user->id)->get();
        return view('admin.agents-register', ['managers' => $managers]);
    }
    public function registerAgent(Request $request){
        // dd($request);
        $user = Auth()->user();
        $validator = Validator::make($request->all(), [      
            'first_name'  => 'required',
            'last_name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:6,max:16',
            'confirm_password' => 'required|same:password|min:6,max:16',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        
        $user = Auth()->user();
        if($request->input('radio-group')=='ABC'){
            $managerId = $user->id;
        }
        else{
            if($request->input('manager_id')){
                if($request->input('manager_id')=='Select'){
                    return redirect()->back()->with("error", "Please select manager");
    
                }
                else{
                    $managerId = $request->input('manager_id');
                }
            }
            else{
                $managerId = $user->id;
            }
        }
        
        
        if ($request->hasFile('profile_image')) {

            $image = $request->file('profile_image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/admin/images");
            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToAdd['profile_image'] = $fullImagePath;
        }
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['role_id'] = '3';
        $dataToAdd['first_name'] = $request->input('first_name');
        $dataToAdd['last_name'] = $request->input('last_name');
        $dataToAdd['phone_number'] = $request->input('phone_number');
        $dataToAdd['email'] = $request->input('email');
        $dataToAdd['password'] = bcrypt($request->input('password'));
        $dataToAdd['address'] = $request->input('address');
        $dataToAdd['country'] = $request->input('country');
        $dataToAdd['state'] = $request->input('state');
        $dataToAdd['city'] = $request->input('city');
        $dataToAdd['zip_code'] = $request->input('zip_code');
        $dataToAdd['status'] = '1';
        $dataToAdd['manager_id'] = $managerId;
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        $add = User::create($dataToAdd);
        if($add){
            return redirect()->back()->with("msg", "Agent added successfully!");
        }
        else{
            return redirect()->back()->with("error", "Could not added user. Try Again!");
        }


    }
    public function activateUser($userId){
        $currentDateAndTime = date('Y-m-d H:i:s');
        $agent = User::find($userId);
        $agent->status = '1';
        $agent->updated_at = $currentDateAndTime;
        $agent->save();
        return redirect()->back()->with("msg", "Agent Successfully Activated!");
    }
    public function deActivateUser(Request $request){
        $userId = $request->input('agent');
        $find = User::find($userId);
        if($find->status == "1"){
            $agent = User::where('id', $userId)->update(['status'=>'0']);
            if($agent){
                return redirect()->back()->with("msg", "Agent Successfully Inactivated!");
            }
            else{
                return redirect()->back()->with("error", "Could not inactive user. Try Again!");
            }
        }
        else{
            $agent = User::where('id', $userId)->update(['status'=>'1']);
            if($agent){
                return redirect()->back()->with("msg", "Agent Successfully Activated!");
            }
            else{
                return redirect()->back()->with("error", "Could not active user. Try Again!");
            }

        }
    }
    public function rejectUser(Request $request){

        $currentDateAndTime = date('Y-m-d H:i:s');
        $agent = User::find($request->input('manager_id'));
        $agent->status = '3';
        $agent->rejection_reason = $request->input('rejection_reason');
        $agent->updated_at = $currentDateAndTime;
        $agent->save();
        return $agent;
    }
    // public function agentProfile($agentId){
    //     return $result = DB::table('users as u1') 
    //     ->leftJoin('users as u2','u1.id', '=', 'u2.manager_id')
    //     ->where('u1.id',$agentId)->get();
    // }
    public function editAgentProfile($agentId){
        return $agent = User::find($agentId);
    }
    public function updateAgentProfile(Request $request){
        $agent = User::find($request->input('agent_id'));
        $input = $request->all();
        $dataToUpdate['first_name'] = $input['first_name'];
        $dataToUpdate['last_name']  =  $input['last_name'];

        if (isset($input['phone_number'])) {
            $dataToUpdate['phone_number'] = $input['phone_number'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('phone_number', $input);
            if ($checkNull) {
                $dataToUpdate['phone_number'] = "";
                $updateProfileCheck = true;
            }
        }

        if (isset($input['address'])) {
            $dataToUpdate['address'] = $input['address'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('address', $input);
            if ($checkNull) {
                $dataToUpdate['address'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['country'])) {
            $dataToUpdate['country'] = $input['country'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('country', $input);
            if ($checkNull) {
                $dataToUpdate['country'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['state'])) {
            $dataToUpdate['state'] = $input['state'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('state', $input);
            if ($checkNull) {
                $dataToUpdate['state'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['city'])) {
            $dataToUpdate['city'] = $input['city'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('city', $input);
            if ($checkNull) {
                $dataToUpdate['city'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['zip_code'])) {
            $dataToUpdate['zip_code'] = $input['zip_code'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('zip_code', $input);
            if ($checkNull) {
                $dataToUpdate['zip_code'] = "";
                $updateProfileCheck = true;
            }
        }

        if ($request->hasFile('profile_image')) {

            $image = $request->file('profile_image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/admin/images");

            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToUpdate['profile_image'] = $fullImagePath;
        }

        $update = User::update($dataToUpdate);
        if($update){
            return redirect();
        }


    }
    public function managers(Request $request){
        $userModel  = new User;

        $managers = User::join('roles', 'users.role_id', '=', 'roles.id')
        ->leftJoin('payment_logs', 'users.id', '=', 'payment_logs.user_id')
        ->where('roles.name', 'manager')
        ->whereNotIn('users.status', ['0', '3'])
        ->select('users.*')->addSelect('roles.*')->addSelect('payment_logs.*')->addSelect('users.id as user_id', 'users.created_at as created_at')->get();

        return view('admin.managers', ['data' => $managers]);
    }
    
    public function viewManagerRecievedRequests($managerId){
        $manager = User::where('id', $managerId)->get();
        return view('admin.recieved-request-details', ['data' => $manager]);
    }

    public function myProfile(){
        $user = Auth()->user();
        return view('admin.user-profile');
    }
    public function myProfileEdit(){
        return view('admin.user-profile-edit');
    }
    public function updateMyProfile(Request $request){
        $validator = Validator::make($request->all(), [      
            'first_name'  => 'required',
            'last_name' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $authUser  = Auth()->user();
        $input = $request->all();
        $dataToUpdate['first_name'] = $input['first_name'];
        $dataToUpdate['last_name']  =  $input['last_name'];

        if (isset($input['phone_number'])) {
            $dataToUpdate['phone_number'] = $input['phone_number'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('phone_number', $input);
            if ($checkNull) {
                $dataToUpdate['phone_number'] = "";
                $updateProfileCheck = true;
            }
        }

        if (isset($input['address'])) {
            $dataToUpdate['address'] = $input['address'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('address', $input);
            if ($checkNull) {
                $dataToUpdate['address'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['country'])) {
            $dataToUpdate['country'] = $input['country'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('country', $input);
            if ($checkNull) {
                $dataToUpdate['country'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['state'])) {
            $dataToUpdate['state'] = $input['state'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('state', $input);
            if ($checkNull) {
                $dataToUpdate['state'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['city'])) {
            $dataToUpdate['city'] = $input['city'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('city', $input);
            if ($checkNull) {
                $dataToUpdate['city'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['zip_code'])) {
            $dataToUpdate['zip_code'] = $input['zip_code'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('zip_code', $input);
            if ($checkNull) {
                $dataToUpdate['zip_code'] = "";
                $updateProfileCheck = true;
            }
        }

        if ($request->hasFile('profile_image')) {

            $image = $request->file('profile_image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/admin/images");

            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToUpdate['profile_image'] = $fullImagePath;
        }

        $update = User::where('id', $authUser->id)->update($dataToUpdate);
        if($update){
            return redirect()->back()->with('success', 'Profile updated successfully.');
        }

    }
    public function changePassword (Request $request) {
            $validator = Validator::make($request->all(), [      
                'old_password'  => [
                    'required',
                    'min:6', 'max:16'
                ],
                'new_password' => [
                    'required',
                    'min:6', 'max:16'
                ],
                'confirm_password' => [
                    'required',
                    'min:6', 'max:16'
                ],

        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }


        $authUser = Auth()->user();
        $currentDateAndTime = date('Y-m-d H:i:s');
        $input              = $request->all();
        $oldPassword        = $input['old_password'];
        $newPassword        = $input['new_password'];
              
        if($oldPassword == $newPassword){
            return redirect()->back()->with('error', 'New password cannot be same as current password.');
        }
        if($newPassword != $input['confirm_password']){
            return redirect()->back()->with('error', 'New password and confirm password do not match.');
        }

        $userOldPassword    = $authUser->password;
        $passwordIsSame     = Hash::check($oldPassword, $userOldPassword);        
        if(!$passwordIsSame){
            return redirect()->back()->with('error', 'Current password is incorrect.');          
        }
        
        //update user password
        $password           = bcrypt($newPassword);
        $userUpdate['password']    =   $password;
        $userUpdate['updated_at']  =   $currentDateAndTime;
        
        $updateUserPassword = User::where('id', $authUser->id)->update(['password' => $password, 'updated_at' => $currentDateAndTime]);

        return redirect()->back()->with('success', 'Password Changed Successfully.');
    }   
    public function updateUserStatus(Request $request){
        //return $request->input('manager-id-input');
         $user = User::findOrFail($request->input('manager-id-input'));
        if($user->status == "1"){
            $user->status = "0";
            $msg = 'Manager deactivated successfully';
        }
        elseif($user->status == "0"){
            $msg = 'Manager activated successfully';
            $user->status = "1";
        }
        $user->save();
        return redirect()->back()->with('success', $msg);

    }
    public function managerProfile($id){
        $user = User::findOrFail($id);
        return view('admin.manager-profile', ['data' => $user]);

    }
    public function managerEditProfile($id){
        $user = User::findOrFail($id);
        return view('admin.manager-edit-profile', ['data' => $user]);
    }
    public function managerUpdateProfile(Request $request){
        $validator = Validator::make($request->all(), [      
            'first_name'  => 'required',
            'last_name' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }

        $input = $request->all();
        $dataToUpdate['first_name'] = $input['first_name'];
        $dataToUpdate['last_name']  =  $input['last_name'];

        if (isset($input['phone_number'])) {
            $dataToUpdate['phone_number'] = $input['phone_number'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('phone_number', $input);
            if ($checkNull) {
                $dataToUpdate['phone_number'] = "";
                $updateProfileCheck = true;
            }
        }

        if (isset($input['address'])) {
            $dataToUpdate['address'] = $input['address'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('address', $input);
            if ($checkNull) {
                $dataToUpdate['address'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['country'])) {
            $dataToUpdate['country'] = $input['country'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('country', $input);
            if ($checkNull) {
                $dataToUpdate['country'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['state'])) {
            $dataToUpdate['state'] = $input['state'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('state', $input);
            if ($checkNull) {
                $dataToUpdate['state'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['city'])) {
            $dataToUpdate['city'] = $input['city'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('city', $input);
            if ($checkNull) {
                $dataToUpdate['city'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['zip_code'])) {
            $dataToUpdate['zip_code'] = $input['zip_code'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('zip_code', $input);
            if ($checkNull) {
                $dataToUpdate['zip_code'] = "";
                $updateProfileCheck = true;
            }
        }

        if ($request->hasFile('profile_image')) {

            $image = $request->file('profile_image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/admin/images");

            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToUpdate['profile_image'] = $fullImagePath;
        }

        $update = User::where('id', $input['user_id'])->update($dataToUpdate);
        if($update){
            return redirect()->back()->with('success', 'Manager updated successfully.');
        }

        
    }

    public function managersRequests(){
        $managersRequests = User::where('status', '0')->where('role_id', '2')->get();
        $managersRejectedRequests = User::where('status', '3')->where('role_id', '2')->get();
        $data['recieved_managers'] = $managersRequests;
        $data['rejected_managers'] = $managersRejectedRequests;
        return view('admin.managers-requests', ['data'=> $data]);

    }

    public function inActiveManagers(){
        $managersRequests = User::where('status', '0')->where('role_id', '2')->get();
        return view('admin.inactive-managers', ['data'=> $managersRequests]);
    }
    public function createManager(){
        return view('admin.register-manager');
    }
    public function addManager(Request $request){
        $validator = Validator::make($request->all(), [      
            'first_name'  => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:6,max:16',
            'confirm_password' => 'required|same:password|min:6,max:16'

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        if ($request->hasFile('profile_image')) {

            $image = $request->file('profile_image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/admin/images");

            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToAdd['profile_image'] = $fullImagePath;
        }
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['first_name'] = $request->input('first_name');
        $dataToAdd['last_name'] = $request->input('last_name');
        $dataToAdd['phone_number'] = $request->input('phone_number');
        $dataToAdd['email'] = $request->input('email');
        $dataToAdd['password'] = bcrypt($request->input('password'));
        $dataToAdd['address'] = $request->input('address');
        $dataToAdd['country'] = $request->input('country');
        $dataToAdd['state'] = $request->input('state');
        $dataToAdd['city'] = $request->input('city');
        $dataToAdd['zip_code'] = $request->input('zip_code');
        $dataToAdd['manager_id'] = 0;
        $dataToAdd['role_id'] = 2;
        $dataToAdd['status'] = '1';
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        $dataToAdd['fee_enabled'] = $request->input('fee_enabled')??0;
        $add = User::create($dataToAdd);
        return redirect()->route('managers.index')->with('success', 'Manager added successfully.');
    }
    public function viewAssignedAgents($id){
        $manager = User::findOrFail($id);
        $agents  = User::where('manager_id', $manager->id)->whereNotIn('status',['2'])->get();
        $data['managerData'] = $manager;
        $data['agentsData'] = $agents;
        return view('admin.view-assigned-agents', ['data' => $data]);
    }

    public function sendPasswordRecoveryCode($email){
        $findUser = User::where('email', $email)->first();
        if(!$findUser){
            return array('msg' => 'Email does not exists');
        }
        $userEmail = $findUser->email;
        $digits = 4;
        $verificationCode       = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        Mail::raw("REACH email recovery code is: $verificationCode", function ($message) use ($userEmail) {
            $message->to($userEmail)
                ->subject('REACH email recovery - REACH')->from('salcogen2021@gmail.com');
        });
        $userModel = new User;
        $data['verification_code'] = $verificationCode;
        $update = $userModel->updateUser($findUser->id, $data);

        return array('msg' => 'Code is sent successfully');

    }

    public function checkPasswordRecoveryCode($email, $code){
        $check = User::where(['email' => $email, 'verification_code' => $code])->first();
        if(!$check){
            return array('msg' => 'Invalid code');
        }
        return array('msg'=> 'code matched');
    }
    public function passwordRecover(Request $request){

        $validator = Validator::make($request->all(), [      
            'password'  => [
                'required',
                'min:6', 'max:16'
            ],
            'confirm_password' => [
                'required',
                'min:6', 'max:16',
            ],
            'recovery-email'  => [
                'required', 'email'
            ],
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        if($request->input('password') != $request->input('confirm_password')){
            return redirect()->back()->with('error', 'Confirm password do not match.');

        }
        $currentDateAndTime = date('Y-m-d H:i:s');
        $password           = bcrypt($request->input('password'));
        $userUpdate['password']    =   $password;        
        $userUpdate['updated_at']  =   $currentDateAndTime;
        
        $updateUserPassword = User::where('email', $request->input('recovery-email'))->update(['password' => $password, 'updated_at' => $currentDateAndTime]);

        return redirect()->back()->with('success', 'Password Changed Successfully.');

    }
    //agents
    public function agentProfile($id){

        $agent = User::where(['id' => $id, 'role_id' => 3])->first();
        $agentManager = User::where(['id' => $agent->manager_id, 'role_id' => 2])->first();
        $data['agentData'] = $agent;
        $data['agentDetailData'] = $agentManager;
        return view('admin.agent-profile', ['data' => $data]);

    }

    public function agentEditProfile($id){
        $user = User::findOrFail($id);
        return view('admin.agent-edit-profile', ['data' => $user]);
    }
    public function agentUpdateProfile(Request $request){
        $validator = Validator::make($request->all(), [      
            'first_name'  => 'required',
            'last_name' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }

        $input = $request->all();
        $dataToUpdate['first_name'] = $input['first_name'];
        $dataToUpdate['last_name']  =  $input['last_name'];

        if (isset($input['phone_number'])) {
            $dataToUpdate['phone_number'] = $input['phone_number'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('phone_number', $input);
            if ($checkNull) {
                $dataToUpdate['phone_number'] = "";
                $updateProfileCheck = true;
            }
        }

        if (isset($input['address'])) {
            $dataToUpdate['address'] = $input['address'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('address', $input);
            if ($checkNull) {
                $dataToUpdate['address'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['country'])) {
            $dataToUpdate['country'] = $input['country'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('country', $input);
            if ($checkNull) {
                $dataToUpdate['country'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['state'])) {
            $dataToUpdate['state'] = $input['state'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('state', $input);
            if ($checkNull) {
                $dataToUpdate['state'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['city'])) {
            $dataToUpdate['city'] = $input['city'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('city', $input);
            if ($checkNull) {
                $dataToUpdate['city'] = "";
                $updateProfileCheck = true;
            }
        }
        if (isset($input['zip_code'])) {
            $dataToUpdate['zip_code'] = $input['zip_code'];
            $updateProfileCheck = true;
        }

        else {
            $checkNull =  array_key_exists('zip_code', $input);
            if ($checkNull) {
                $dataToUpdate['zip_code'] = "";
                $updateProfileCheck = true;
            }
        }

        if ($request->hasFile('profile_image')) {

            $image = $request->file('profile_image');
            $extension = $image->getClientOriginalExtension();
            $ImgName = $image->getClientOriginalName();
            $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            $destinationPath = public_path("/assets/admin/images");

            if (!file_exists($destinationPath)) {
                //create folder
                mkdir($destinationPath, 0755, true);
            }

            $time = time();
            $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $dataToUpdate['profile_image'] = $fullImagePath;
        }

        $update = User::where('id', $input['agent_id'])->update($dataToUpdate);
        if($update){
            return redirect()->back()->with('success', 'Agent profile updated successfully');
        }

        
    }

    public function recievedRequestDetails($managerId){
        $data= User::findOrFail($managerId);
        return view('admin.recieved-request-details', ['data' => $data]);
    }
    public function rejectManagersRequest(Request $request){
        $find = User::findOrFail($request->input('manager_id'));
        if($request->input('type') == 'reject'){
            $find->status = '3';
            $find->rejection_reason = $request->input('reason');
            $msg = 'Manager request rejected successfully.';
        }
        else{
            $find->status = '1';
            $msg = 'Manager request accepted successfully.';
        }
        $find->save();
        return redirect()->route('managersRequests')->with('success', $msg);

    }

    public function rejectedRequestDetails($managerId){
        $data = User::findOrFail($managerId);
        return view('admin.rejected-request-details', ['data' => $data]);
    }
    public function checkEmail($email){
        $data = User::where(['email' => $email, 'role_id'=>1])->first();
        if($data){
            return array('msg' => 'email_found');

        }
        return array('msg' => 'email_not_found');

    }

}
