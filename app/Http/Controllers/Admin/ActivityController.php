<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserActivity;
use App\Models\Activity;
use App\Models\Category;
use DB;
use Validator;
use App\Models\User;
class ActivityController extends Controller
{
    public function index(Request $request){
     
        $data = Activity::leftJoin('categories', 'categories.id', '=', 'activities.category_id')->select('activities.*')->addSelect('categories.*')->addSelect('activities.id as activity_id')->addSelect('activities.name as activity_name')->addSelect('activities.description as activity_description')->addSelect('categories.name as category_name')->addSelect('activities.status as activity_status')->get();
        $cats = Category::where('status', '1')->get();
        return view('admin.activity-management', ['data' => $data, 'cats'=> $cats]);
    }
    public function assignAgentActivity(Request $request){

        $validator = Validator::make($request->all(), [      
            'activity_id'  => 'required',
            'goal'         => 'required'

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $agentId = $request->input('agent_id');
        $activityId = $request->input('activity_id');
        $goal = $request->input('goal');
        $currentDateAndTime = date('Y-m-d H:i:s');

        $dataToAdd['user_id'] = $agentId;
        $dataToAdd['activity_id'] = $activityId;
        $dataToAdd['status']    = '0';
        $dataToAdd['goal']    = $goal;
        $dataToAdd['create_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        $add = UserActivity::create($dataToAdd);
        if($add){
            return redirect()->back()->with('msg', 'Activity assinged to agent successfully.');
        }
        else{
            return redirect()->back()->with('fail', 'Could not add activity! Try Again.');
        }

    }
    public function findActivitiesByCatId($d){
        return $acts = Activity::where(['category_id' =>  $id, 'status' => '1'])->get();
    }
    public function createActivityManagement(){
        $cats = Category::where('status', '1')->get();
        return view('admin.add-activity-management', ['data' => $cats]);
    }
    public function addActivity(Request $request){
        $validator = Validator::make($request->all(), [      
            'activity_name'  => 'required',
            'activity_des' => 'required',
            'status' => 'required',
            'category_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['name'] = $request->input('activity_name');
        $dataToAdd['description'] = $request->input('activity_des');
        $dataToAdd['status'] = $request->input('status');
        $dataToAdd['category_id'] = $request->input('category_id');
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        $add = Activity::create($dataToAdd);
        if($add){
            return redirect()->route('activity.management.index')->with('success', 'Activity Added Successfully.');
        }
        else{
            return redirect()->route('activity.management.index')->with('fail', 'Could not add activity! Try Again.');
        }
    }
        
    public function updateActivity(Request $request){
        $validator = Validator::make($request->all(), [      
            'activity-name'  => 'required',
            'activity-description' => 'required',
            'status' => 'required',
            'category-id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $activity = Activity::findOrFail($request->input('activity-id'));
        if(!$request->input('category-id')){$catId = $activity->category_id;}
        else{$catId = $request->input('category-id');}

        $activity->name = $request->input('activity-name');
        $activity->description = $request->input('activity-description');
        $activity->category_id = $catId;
        $activity->status = $request->input('status');
        $activity->save();
        if($activity){
            return redirect()->back()->with('success', 'Activity Updated Successfully.');
        }
        else{
            return redirect()->back()->with('fail', 'Could not update activity! Try Again.');
        }
    }
    public function getActivitiesByCatId($id){
        $activities = Activity::where('category_id', $id)->get();
        
        if(!count($activities)){
            return array();
        }
        $temp = '';
        foreach($activities as $activity){

            $temp .= "<option value='$activity->id'>$activity->name</option>";
        }
        return $temp;
        
    }
    public function viewAssignedActivities($id){
        $data['activityData'] = UserActivity::leftJoin('activities', 'activities.id', '=', 'user_activities.activity_id')
        ->where('user_activities.user_id', $id)
        ->select('user_activities.*')->addSelect('activities.*')->addSelect('user_activities.id as user_activity_id')
        ->addSelect('user_activities.created_at as created_at')
        ->get();
        $data['userData'] = User::find($id);
        return view('admin.view-assigned-activities', ['data' => $data]);
        
    }
    public function assignedTasksDetails($id){
        $data['activityData'] = UserActivity::leftJoin('activities', 'activities.id', '=', 'user_activities.activity_id')        
        ->where('user_activities.id', $id)
        ->select('user_activities.*')->addSelect('activities.*')->addSelect('user_activities.id as user_activity_id')
        ->first();
        $data['userData'] = User::find($data['activityData']->user_id);
        return view('admin.assigned-tasks-details', ['data' => $data]);

    }
}
