<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Category;
use DB;


class NotificationController extends Controller
{
    public function getMyNotifications(Request $request){
        $to = $request->query('to');
        $from = $request->query('from');
        $authUser = Auth()->user();
        
        $not = Notification::where('user_id', $authUser->id);
        if($to){
            $not->where('notifications.created_at', '>=', $to);
        }
        if($from){
            $not->where('notifications.created_at', '<=', $from);
        }
        $not = $not->orderBy('notifications.id', 'DESC')->get();
        return view('admin.notifications', ['data' => $not]);
    }
}
