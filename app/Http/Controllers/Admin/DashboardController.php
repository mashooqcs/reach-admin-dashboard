<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class DashboardController extends Controller
{
    public function index(){
        $authUser = Auth()->user();
        $userModel = new User;
        $agents     = $userModel->leftJoin('roles', 'roles.id', '=', 'users.role_id')->where('roles.name', 'agent')->where('users.status', '1')->count();
        $managers   = $userModel->leftJoin('roles', 'roles.id', '=', 'users.role_id')->where('roles.name', 'manager')->where('users.status', '1')->count();
        $myAgents   = $userModel->leftJoin('roles', 'roles.id', '=', 'users.role_id')->where('roles.name', 'agent')->where('users.status', '1')->where('users.manager_id', $authUser->id)->count();
        $data['agents'] = $agents;
        $data['managers'] = $managers;
        $data['myAgents'] = $myAgents;
    
        return view('admin.dashboard', ['data' => $data]);
    }
}
