<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
class UserActivityController extends Controller
{
    public function viewAssignedActivities(Request $request){
        $typeBy = 'hourly';
        $paginate   = $request->query('per-page');
        $to   = $request->query('to');
        $from  = $request->query('from');

        if(!$paginate){
            $paginate = 20;
        }
        if($request->query('type')){
            $typeBy     = $request->query('type');
        }
        $search     = $request->query('search');
        $activities = UserActivity::leftJoin('users', 'users.id', '=', 'user_activities.user_id')
        ->leftJoin('activities', 'activities.id', '=', 'user_activities.activity_id')
        ->where(function ($query) use($to, $from){     
            $query->where('users.created_at', '>=', $from)-where('users.created_at', '<=', $from);
        })->paginate($paginate);

        return view('admin.view-assigned-activities', ['data' => $activities]);

    }
    public function viewAssignedActivityTaskDetail($id){
        $data = UserActivity::leftJoin('activities', 'user_activities.activity_id', '=', 'activities.id')->where('user_activities.id', $id)->first();
        return view('admin.assigned-tasks-detail', ['data' => $data]);
    }
    public function viewMonthlyProgress($agentId){
        $data['userData'] = User::find($agentId);
        return view('admin.monthly-progress', ['data' => $data]);

    }

}
