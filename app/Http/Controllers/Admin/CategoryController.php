<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

use Validator;
class CategoryController extends Controller
{
    public function index(){
        $cats = Category::where('status', '1')->orderBy('id', 'DESC')->get();
        return view('admin.category-management' , ['data' => $cats]);
    }
    public function addCategory(Request $request){
        $validator = Validator::make($request->all(), [      
            'cat_name'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['name']     = $request->input('cat_name');
        $dataToAdd['status']        = '1';
        $dataToAdd['created_at']    = $currentDateAndTime;
        $dataToAdd['update_at']     =  $currentDateAndTime;
        $add = Category::create($dataToAdd);
        if($add){
            return redirect()->back()->with('success', 'Category Added Successfully.');
        }
        else{
            return redirect()->back()->with('fail', 'Could not add category! Try Again.');
        }
    }
    public function updateCategory(Request $request){
        $validator = Validator::make($request->all(), [      
            'category_name'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $cat = Category::find($request->input('category_id'));
        $cat->name =   $request->input('category_name');
        $cat->save();
        if($cat){
            return redirect()->back()->with('success', 'Category Updated Successfully.');
        }
        else{
            return redirect()->back()->with('fail', 'Could not update category! Try Again.');
        }

    }
    
}
