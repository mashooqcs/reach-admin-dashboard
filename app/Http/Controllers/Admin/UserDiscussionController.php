<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserDiscussion;
use App\Models\User;
use DB;
use Validator;


class UserDiscussionController extends Controller
{
    public function discussionBoard($userId){
        $authUser = Auth()->user();
        $data['discussionData'] = UserDiscussion::where('sender_id', $userId)->orWhere('recipient_id', $userId)->get();
        $data['userData'] = User::find($userId);
        return view('admin.discussion-board', ['data' => $data]);

    }
    public function addDiscussion(Request $request){
        $validator = Validator::make($request->all(), [      
            'message'  => 'required',
            'user_id'  => 'required',
            

        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $authUser = Auth()->user();
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['message'] = $request->input('message');
        $dataToAdd['sender_id'] = $authUser->id;
        $dataToAdd['recipient_id'] = $request->input('user_id');
        $dataToAdd['sender_type'] = 'admin';
        $dataToAdd['recipient_type'] = 'agent';
        $userDiscussionModel = new UserDiscussion;
        $add = $userDiscussionModel->store($dataToAdd);
        if($add){
            return redirect()->back()->with('success', 'Message sent successfully.');
        }
        else{
            return redirect()->back()->with('error', 'Error sending message.');
        }
    }
    
}
