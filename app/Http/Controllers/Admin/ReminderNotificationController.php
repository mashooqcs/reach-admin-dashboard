<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReminderNotification;
use DB;
use Validator;
use Hash;
class ReminderNotificationController extends Controller
{
    public function addReminder(Request $request){
        $validator = Validator::make($request->all(), [      
            'title'  => 'required',
            'start_date' => 'required|date_format:m/d/Y',
            'start_time' => 'required|date_format:H:i',
            'repeat_on' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $user = Auth()->user();
        //$date = str_replace('/', '-', $request->input('start_date'));
        $date = explode('/',$request->input('start_date'));
        $month = $date[0];
        $day = $date[1];
        $year = $date[2];
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['title']         = $request->input('title');
        $dataToAdd['user_id']       = $user->id;
        $dataToAdd['start_date']    = $year."-".$month."-".$day;
        $dataToAdd['start_time']    = $request->input('start_time');
        $dataToAdd['repeat_on']     = $request->input('repeat_on');
        $dataToAdd['created_at']    = $currentDateAndTime;
        $dataToAdd['updated_at']    = $currentDateAndTime;

        $add = ReminderNotification::create($dataToAdd);
        return redirect()->back()->with("success", "Reminder added successfully!");
    }
}
