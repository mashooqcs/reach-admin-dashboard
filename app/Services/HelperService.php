<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use App\Models\User;
use App\Models\Activity;
use App\Models\ReminderNotification;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HelperService
{
    
    public static function getManagerName($id)
    {
        $user =  User::find($id);
        $fullname =  $user->first_name ." " . $user->last_name;
        return $fullname;
    }
    public static function getActivityCount($catId){
        return Activity::where('category_id', $catId)->count();
    }

}