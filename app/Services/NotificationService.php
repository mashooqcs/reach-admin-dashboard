<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Notification;
use App\Models\ReminderNotification;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class NotificationService
{
    public static function getRecentNotifications()
    {
        $authUser           = Auth()->user();
        $userId             = $authUser->id;
        return $get = Notification::where('user_id', $userId)->limit(5)->get();
    }
    public static function getReminders()
    {
        $authUser           = Auth()->user();
        $userId             = $authUser->id;
        $date = date('Y-m-d'); 
        $active = ReminderNotification::where('user_id', $userId)->where('start_date', '=', $date)->get();
        $inactive = ReminderNotification::where('user_id', $userId)->where('start_date', '<', $date)->get();
        return array('active' => $active, 'inactive' => $inactive);

    }
    public static function dateTimeFormat($dateTime, $format = 'M d, Y h:i A', $locale = null, $default = '')
    {
        if ($locale != null) {
            Carbon::setLocale($locale);
        }

        try {
            return Carbon::parse($dateTime)->format($format);
        } catch (\Exception $err) {
            return $default;
        }
    }
    public static function dateFormat($dateTime, $format = 'M d, Y', $locale = null, $default = '')
    {
        if ($locale != null) {
            Carbon::setLocale($locale);
        }

        try {
            return Carbon::parse($dateTime)->format($format);
        } catch (\Exception $err) {
            return $default;
        }
    }
    public static function timeFormat($dateTime, $format = 'h:i A', $locale = null, $default = '')
    {
        if ($locale != null) {
            Carbon::setLocale($locale);
        }

        try {
            return Carbon::parse($dateTime)->format($format);
        } catch (\Exception $err) {
            return $default;
        }
    }


}