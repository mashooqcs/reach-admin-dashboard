<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use DB;
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;
    //define constants here
    const USER_STATUS_INACTIVE          = "0";
    const USER_STATUS_ACTIVE            = "1";
    const USER_STATUS_BLOCKED           = "2";
    const USER_STATUS_DELETED           = "3";
    const USER_ROLE_ADMIN               = "1";
    const USER_ROLE_MANAGER             = "2";
    const USER_ROLE_AGENT               = "3";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'manager_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'profile_image',
        'phone_number',
        'address',
        'country',
        'state',
        'city',
        'zip_code',
        'fee_enabled',
        'status',
        'rejection_reason',
        'verification_code',
        'verification_code_expiry',
        'device_id',
        'device_type',
        'timezone'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }


    public function findUserByEmail($email, $roleId){
        return $user = User::where(['email' => $email, 'role_id' => $roleId])->first();
    }
    public function updateUser($userId, $data){
        return $updateUser = User::where('id', $userId)->update($data);
    }
    public function getAgents($fromDate, $toDate, $keyword, $limit, $offset){
        $user       = Auth::user();
        $userId     = $user->id;
        
        $agents = User::where(['role_id' => self::USER_ROLE_AGENT, 'status' => self::USER_STATUS_ACTIVE,  'manager_id' =>$userId ])
        ->where(function ($query) use ($keyword) {
            $columns = ['first_name', 'last_name', DB::raw("concat(first_name, ' ', last_name)")];
            foreach ($columns  as $column) {
                $query->orWhere($column, 'like', "%{$keyword}%");
            }
        });
        $count = count($agents->get());    
    
       return array('data' => $agents->limit($limit)->offset($offset)->select('id as agent_id', 'first_name', 'last_name', 'created_at as registration_date')->get(), 'count'=> $count);
       

    }
    public function findUserById($userId){
        return $user = User::where(['id' => $userId, 'status' => self::USER_STATUS_ACTIVE])->first();
    }
}
