<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    const USER_SUBSCRIPTION_STATUS_ACTIVE   ="1";
    const USER_SUBSCRIPTION_STATUS_INACTIVE ="0"; 
    use HasFactory;
    protected $fillable = [
        'user_id', 'registration_fee', 'card_holder_name', 'card_number', 'expiry_month', 'expiry_year', 'cvv', 'status'
    ];
    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
    public function getUserSubscription($userId){
        return $getUserSubscription = UserSubscription::where(['user_id' => $userId, 'status' => self::USER_SUBSCRIPTION_STATUS_ACTIVE])->orderBy('id', 'DESC')->first();
    }
    public function addSubscription($userId, $data){
        $updatePre = UserSubscription::where(['user_id' => $userId, 'status' => self::USER_SUBSCRIPTION_STATUS_ACTIVE])->update(['status' => self::USER_SUBSCRIPTION_STATUS_INACTIVE]);
        return $getUserSubscription = UserSubscription::create($data);
    }

}
