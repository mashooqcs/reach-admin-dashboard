<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id','fee','payment_id', 'status'
    ];
    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
    public function getPaymentLogs($userId, $limit, $offset){
        $log = PaymentLog::join('users', 'users.id', '=', 'payment_logs.user_id')->where('payment_logs.user_id', $userId)->select('users.*')->select('payment_logs.*')->addSelect('payment_logs.fee as amount', 'payment_logs.created_at as payment_date', 'users.created_at as registration_date');
        $count = count($log->get());
        $log = $log->limit($limit)->offset($offset)->orderBy('payment_logs.id', 'DESC')->get();
        return array('data' => $log, 'count'=> $count);

    }
}
