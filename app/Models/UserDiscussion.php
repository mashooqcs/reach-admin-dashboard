<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDiscussion extends Model
{
    use HasFactory;
    protected $fillable = [
        'sender_id', 'recipient_id', 'message', 'status', 'sender_type', 'recipient_type'
    ];
    protected $appends = ['owner'];

    public function getOwnerAttribute(){

        return (Auth()->user()->id == $this->sender_id);        
    }

    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
    public function store($data){
        return $store = UserDiscussion::create($data);
    }
}
