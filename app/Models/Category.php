<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //define constants here
    const CATEGORY_STATUS_ACTIVE    = "1";
    const CATEGORY_STATUS_INACTIVE  = "0";


    use HasFactory;
    protected $fillable = [
        'name', 'status'
    ];
    public function getAllCategories($limit, $offset){
        $cats =  Category::where('status', self::CATEGORY_STATUS_ACTIVE)->orderBy('id', 'DESC');
        $count = count($cats->get());
        $cats = $cats->limit($limit)->offset($offset)->orderBy('id', 'DESC')->get();
        return array('data' => $cats, 'count'=> $count);

    }
    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
}
