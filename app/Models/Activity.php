<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    const ACTIVITY_STATUS_ACTIVE = "1";
    use HasFactory;
    protected $fillable = [
        'category_id', 'name', 'description', 'status'
    ];
    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
    public function findActivitiesByCategory($categoryId, $limit, $offset){
        $acts = Activity::where(['category_id' => $categoryId, 'status' => self::ACTIVITY_STATUS_ACTIVE]);
        $count = count($acts->get());
        $acts = $acts->limit($limit)->offset($offset)->orderBy('id', 'DESC')->get();
        return array('data' => $acts, 'count'=> $count);

    }
    public function getAgentActivities($agentId, $limit, $offset, $hourly, $numeric, $fromDate, $toDate, $keyword){
        $acts = Activity::join('categories', 'categories.id', '=', 'activities.category_id')->leftJoin('user_activities', 'user_activities.activity_id', '=', 'activities.id')->select('activities.id as activity_id', 'activities.name as activity_name', 'activities.created', 'activities.description')->addSelect('categories.name as category_name')->where(['activities.status' => self::ACTIVITY_STATUS_ACTIVE])->where(function($query) use($hourly, $numeric, $fromDate, $toDate, $keyword){
            if($hourly){
                $query->where('categories.name', 'hourly');
            }
            if($numeric){
                $query->where('categories.name', 'numeric');
            }
            if($fromDate){
                $query->where('activities.created_at',  $fromDate);
            }
            if($toDate){
                $query->where('activities.created_at', $toDate);
            }
            if($keyword){
                $query->where('activities.name', 'like', '%'.$keyword.'%');
            }            
        });
        $count = count($acts->get());
        $acts = $acts->limit($limit)->offset($offset)->orderBy('activities.id', 'DESC')->get();
        return array('data' => $acts, 'count'=> $count);
    }
}
