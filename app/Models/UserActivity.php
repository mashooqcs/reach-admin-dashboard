<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'activity_id', 'assign_date', 'start_date', 'goal', 'status'
    ];
    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
    public function assignAgentActivity($data){
        return $assign = UserActivity::create($data);
    }
}
