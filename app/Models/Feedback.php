<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id','email', 'first_name', 'last_name', 'message', 'subject', 'status'
    ];
    public function getCreatedAtAttribute($dateTime)
    {
        $date = $this->asDateTime($dateTime);
        return $date->timezone(auth()->user()->timezone)->toDateTimeString();
    }
    public function addFeedback($data){
        return $add = Feedback::create($data);
    }
}
