<?php 
$title = 'Agent Profile';
$pg = 'agents';
?>
@extends('admin.layout.app')
@section('title', __('Agent profile'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('agents.index')}}"><i class="fas fa-chevron-left mr-1"></i> AGENTS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>view profile</p>
                                            <p>agent id: {{$data['agentData']->id}}</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <div class="text-info-area">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p><?php echo $notificationService::dateTimeFormat($data['agentData']->created_at);?></p>
                                                    <a href="{{route('agentEditProfile', $data['agentData']->id)}}" class="site-btn sm-btn blue">EDIT</a>
                                            </div>
                                            <div class="profile-picture-div">

                                            @if($data['agentData']->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data['agentData']->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif 
                                
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="{{$data['agentData']->first_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="{{$data['agentData']->last_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number</label>
                                                        <input type="number" id="" class="site-input" placeholder="Enter Phone Number" value="{{$data['agentData']->phone_number}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="{{$data['agentData']->email}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="{{$data['agentData']->address}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="{{$data['agentData']->country}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="{{$data['agentData']->state}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="{{$data['agentData']->city}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223" value="{{$data['agentData']->zip_code}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card-heading">
                                            <p>coach details</p>
                                        </div>
                                        @if($data['agentDetailData'])

                                        <div class="card-area">
                                            <div class="profile-picture-div">
                                                @if($data['agentDetailData']->profile_image)
                                                            <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data['agentDetailData']->profile_image}}" class="profile-pic img-fluid" alt="">
                                                        @else
                                                            <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                        @endif 
                                    
                                                </div>
                                                <p class="form-heading mb-0">Manager Id: {{$data['agentDetailData']->id}}</p>
                                                <p class="form-heading">{{$data['agentDetailData']->first_name}}{{' '}}{{$data['agentDetailData']->last_name}}</p>
                                        </div>
                                        @endif()

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection