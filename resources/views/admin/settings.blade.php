<?php 
$title = 'Settings';
$pg = 'settings';
?>
@extends('admin.layout.app')
@section('title', __('Settings'))
@section('content')

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Settings</h1>
                                            @if(Session::has('success'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success') }}</p>
                                                @elseif(Session::has('error'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                                @endif
                                        </div>
                                       
                                    </div>
                                    <div class="custom-card">  
                                        <div class="card-area full">
                                            <p class="form-heading mb-2">Agent</p>
                                            <div class="row form-field align-items-center">
                                                <div class="col-md-2 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <p class="orange-text lg-text"><?php if($data){ echo '$ '.$data->agent_fee;} ?></p>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <a href="{{route('editSettings')}}" class="site-btn lg-btn blue">Edit</a>
                                                </div>
                                            </div>
                                            <p class="form-heading mb-2">Manager</p>
                                            <div class="row form-field align-items-center">
                                                <div class="col-md-2 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <p class="orange-text lg-text"><?php if($data){ echo '$ '.$data->manager_fee;} ?></p>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <a href="{{route('editSettings')}}" class="site-btn lg-btn blue">Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsecton