<?php 
  $pg='settings';
  $title = "Settings";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Settings</h1>
                                        </div>
                                    </div>
                                    <div class="custom-card">  
                                        <div class="card-area full">
                                            <p class="form-heading mb-2">Agent</p>
                                            <div class="row form-field align-items-center">
                                                <div class="col-md-6 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <p class="orange-text lg-text">$123</p>
                                                </div>
                                                <div class="col-md-6 col-12 text-md-right text-left">
                                                    <a href="edit-settings.php" class="site-btn blue">Edit</a>
                                                </div>
                                            </div>
                                            <p class="form-heading mb-2">Manager</p>
                                            <div class="row form-field align-items-center">
                                                <div class="col-md-6 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <p class="orange-text lg-text">$123</p>
                                                </div>
                                                <div class="col-md-6 col-12 text-md-right text-left">
                                                    <a href="edit-settings.php" class="site-btn blue">Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
