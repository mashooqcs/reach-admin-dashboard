<?php 
  $pg='managers';
  $title = "Manager Profile";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="managers.php"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>view profile</p>
                                            <p>Manager id:001</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <div class="text-info-area mb-2">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p>May 2,2020</p>
                                                    <a href="manager-edit-profile.php" class="site-btn sm-btn blue">EDIT</a>
                                            </div>
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <!-- <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/" id="picture">
                                                        <input type="submit">
                                                    </form> -->
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="Lorem" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="Ipsum" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number</label>
                                                        <input type="number" id="" class="site-input" placeholder="Enter Phone Number" value="232148" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="hamza@gmail" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="12sad23" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="USA" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="Texas" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="abc" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223" value="123" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <p class="form-heading pt-3 mb-3">Address Details</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" placeholder="E.g James">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" placeholder="Eg James">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" placeholder="Abc">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="email" class="site-input" placeholder="Abc">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label for="" class="site-label">Zip Code</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <p class="select-pkg-text black-text">
                                                        <input type="radio" class="pkg-radio" id="test1" name="radio-group" checked="checked" value="ABC">
                                                        <label for="test1">Under My Supervision</label>
                                                    </p> 
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <p class="select-pkg-text black-text">
                                                        <input type="radio" class="pkg-radio" id="test2" name="radio-group" checked="" value="PQR">
                                                        <label for="test2">Under Manager's Supervision</label>
                                                    </p>
                                                </div>
                                                <div class="select-manager" style="display:none;">
                                                    <label for="" class="site-label pl-1">Select Managers</label>
                                                    <select class="manager-dropdown">
                                                        <option selected="selected">Manager1</option>
                                                        <option>Abc</option>
                                                        <option>xyz</option>
                                                        <option>uio</option>
                                                    </select>
                                                </div>
                                                <div class="col-12">
                                                    <p class="black-text mb-0">
                                                        <input type="checkbox" id="stopover" name="radio-group">
                                                        <label for="stopover" class="bordered mb-0">Enable Registration Fee</label>
                                                    </p>
                                                </div>
                                            </div> -->
                                            <!-- <div class="text-center mt-3">
                                                <a data-toggle="modal" data-target=".registerAgent" class="site-btn blue">REGISTER</a>
                                            </div> -->
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<?php include('footer.php') ?>
