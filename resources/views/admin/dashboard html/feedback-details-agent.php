<?php 
  $pg='feedback';
  $title = "Agent Feedback Details";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="feedback.php"><i class="fas fa-chevron-left mr-1"></i> FEEDBACK DETAILS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>feeback</p>
                                            <p>agent id:001</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <div class="text-md-right text-left">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p>May 2,2020</p>

                                            </div>
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <!-- <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        <input type="submit">
                                                    </form> -->
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="Lorem" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="Ipsum" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="hamza@gmail" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Subject*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="When an unknown printer took" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="House A-123 , Block #3 Street #13 , California" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>



<?php include('footer.php') ?>
