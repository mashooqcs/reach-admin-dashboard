<?php 
  $pg='agents';
  $title = "Agents";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">agents</h1>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills agent-pills" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Manager's Agents</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">My Agents</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                            <div class="text-md-right text-left mb-2">
                                                <a href="inactive-agents.php" class="site-btn lg-btn blue">INACTIVE AGENTS</a>
                                                <a href="agents-register.php" class="site-btn lg-btn orange text-uppercase ml-2">register AGENTS</a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>AGENT ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FEE</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abc</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>$123</td>
                                                            <td>002</td>
                                                            <td>Abc</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="payment-detail.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                        <a class="dropdown-item" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a href="agent-profile.php" class="dropdown-item" href="payment-detail.php"><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="view-assigned-activities.php"><i class="fas fa-clipboard-list"></i>View Assigned Activities</a> 
                                                                        <a class="dropdown-item" href="monthly-progress.php"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="discussion-board.php"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>02</td>
                                                            <td>001</td>
                                                            <td>Abc</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>$123</td>
                                                            <td>002</td>
                                                            <td>Abc</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="payment-detail.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                        <a class="dropdown-item" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a href="agent-profile.php" class="dropdown-item" href="payment-detail.php"><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="view-assigned-activities.php"><i class="fas fa-clipboard-list"></i>View Assigned Activities</a> 
                                                                        <a class="dropdown-item" href="monthly-progress.php"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="discussion-board.php"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>03</td>
                                                            <td>001</td>
                                                            <td>Abc</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>$123</td>
                                                            <td>002</td>
                                                            <td>Abc</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="payment-detail.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                        <a class="dropdown-item" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a class="dropdown-item" href="agent-profile.php"><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="view-assigned-activities.php"><i class="fas fa-clipboard-list"></i>View Assigned Activities</a> 
                                                                        <a class="dropdown-item" href="monthly-progress.php"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="discussion-board.php"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="text-md-right text-left mb-2">
                                                    <a href="inactive-agents.php" class="site-btn lg-btn blue">INACTIVE AGENTS</a>
                                                    <a href="agents-register.php" class="site-btn lg-btn orange text-uppercase ml-2">register AGENTS</a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>AGENT ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FULL NAME</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abc</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>Abce</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="payment-detail.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>ACTIVE / INACTIVE </a> 
                                                                        <a class="dropdown-item" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a class="dropdown-item" href="agent-profile2.php"><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="view-assigned-activities.php"><i class="fas fa-clipboard-list"></i>View Assigned tasks</a> 
                                                                        <a class="dropdown-item" href="payment-detail.php"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="discussion-board.php"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>02</td>
                                                            <td>001</td>
                                                            <td>Abc</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>Abce</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="payment-detail.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>ACTIVE / INACTIVE </a> 
                                                                        <a class="dropdown-item" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a class="dropdown-item" href="agent-profile2.php"><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="payment-detail.php"><i class="fas fa-clipboard-list"></i>View Assigned tasks</a> 
                                                                        <a class="dropdown-item" href="payment-detail.php"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="discussion-board.php"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this agent's profile ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent xyz Profile has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <i class="fas fa-search left-icon"></i>
                    <select class="site-input border both-icon" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>

