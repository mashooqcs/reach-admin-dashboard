<?php 
  $pg='managers';
  $title = "Managers";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Managers</h1>
                                        </div>
                                    </div>
                                    <div class="three-btn-div">
                                        <a href="managers-requests.php" class="site-btn lg-btn black">REQUESTS</a>
                                        <a href="inactive-managers.php" class="site-btn lg-btn blue">INACTIVE MANAGERS</a>
                                        <a href="register-manager.php" class="site-btn lg-btn orange text-uppercase">REGISTER MANAGER</a>
                                    </div>
                                    <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FEE</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>$123</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                        <a href="manager-profile.php" class="dropdown-item" href="payment-detail.php"><i class="fas fa-edit"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="view-assigned-agents.php"><i class="fa fa-user-circle" aria-hidden="true"></i>View Assigned Agents</a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this manager's profile ?</p>
                <div class="modal-btn-div">
                    <a href="#" class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Manager XYZ Profile has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>
