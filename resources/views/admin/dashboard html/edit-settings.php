<?php 
  $pg='settings';
  $title = "Settings";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Settings</h1>
                                        </div>
                                    </div>
                                    <div class="custom-card">  
                                        <div class="card-area full">
                                            <p class="form-heading mb-2">Agent</p>
                                            <div class="row form-field align-items-end">
                                                <div class="col-md-4 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <input type="text" class="site-input" value="123" placeholder="Enter Fee">
                                                </div>
                                                <div class="col-md-8 col-12 text-md-right text-left">
                                                    <a href="settings.php" class="site-btn blue mt-md-0 mt-1">Update</a>
                                                </div>
                                            </div>
                                            <p class="form-heading mb-2">Manager</p>
                                            <div class="row form-field align-items-end">
                                                <div class="col-md-4 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <input type="text" class="site-input" value="123" placeholder="Enter Fee">
                                                </div>
                                                <div class="col-md-8 col-12 text-md-right text-left">
                                                    <a href="settings.php" class="site-btn blue mt-md-0 mt-1">Update</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
