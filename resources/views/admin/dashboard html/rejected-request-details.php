<?php 
  $pg='managers';
  $title = "Rejected Request Details";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="managers-requests.php"><i class="fas fa-chevron-left mr-1"></i>REJECTED PROFILE</a></h1>
                                        </div>
                                    </div>
                                    <div class="profile-picture-div">
                                        <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>Personal Information</p>
                                            <p>Verification Request Id:001</p>
                                        </div>
                                      
                                        <div class="card-area">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">First Name</label>
                                                            <p class="site-text">James</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Last Name</label>
                                                            <p class="site-text">Vince</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Phone Number</label>
                                                            <p class="site-text">+1 333 333 4444</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Email Address</label>
                                                            <p class="site-text">James@email.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="card-heading">
                                            <p>Address Details</p>
                                        </div>
                                            <div class="card-area">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Address</label>
                                                            <p class="site-text">Abc street 123 road</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Country</label>
                                                            <p class="site-text">Abc</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">State</label>
                                                            <p class="site-text">Abc</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">City</label>
                                                            <p class="site-text">Abc</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Zip Code</label>
                                                            <p class="site-text">1234</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-field">
                                                    <label for="" class="site-label font-weight-bold">Manager Name</label>
                                                    <p class="site-text font-weight-bold black-text">Mark Wilson</p>
                                                </div>
                                                <div class="form-field">
                                                    <label for="" class="site-label font-weight-bold">Rejection Reason</label>
                                                    <p class="site-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                                                </div>
                                            </div>
                                            </div>           
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Manager Modal -->
<div class="modal fade registerManager" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this Manager?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange yesmanager">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Manager Confirmation -->
<div class="modal fade yesregisterManager" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Manager has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Reason -->
<div class="modal fade rejectionReason" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
            </div>
                <div class="form-field">
                    <label for="" class="site-label font-weight-bold">Enter Rejection Reason</label>
                    <textarea name="" id="" cols="30" rows="10" class="site-input border" placeholder="Enter Rejection Reason"></textarea>
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn orange" data-dismiss="modal" aria-label="Close">SUBMIT</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">CANCEL</a>
                </div>

        </div>
    </div>
</div>

<?php include('footer.php') ?>

