<?php 
  $pg='managers';
  $title = "Managers";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Managers</h1>
                                        </div>
                                    </div>
                                    <div class="three-btn-div">
                                        <a href="managers-requests.php" class="site-btn lg-btn black">REQUESTS</a>
                                        <a href="inactive-agents.php" class="site-btn lg-btn blue">INACTIVE AGENTS</a>
                                        <a href="agents-register.php" class="site-btn lg-btn orange text-uppercase">REGISTER AGENTS</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="empty-div">
                                        <h1>No Data Found</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this agent's profile ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent xyz Profile has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>

