<?php 
    $pg="error";
    $title = "Error404";
    include('header.php');
?>
                <section id="configuration">
                    <div class="row ml-0 mr-0">
                        <div class="col-12 pr-0 pl-0">
                            <div class="card ">
                                <div class="card-content collapse show">
                                  <div class="card-dashboard p-4">
                                    <div class="text-center">
                                        <img src="./images/404-img.png" class="error404 img-fluid" alt="">

                                        <h1 class="grey-text text-capitalize my-2">Page not found</h1>
                                        <a href="dashboard.php" class="site-btn orange lg-link">GO TO HOMEPAGE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

    <?php include('footer.php') ?>