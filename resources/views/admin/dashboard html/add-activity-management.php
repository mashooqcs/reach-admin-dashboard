<?php 
  $pg='agents';
  $title = "Edit Activity Management";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="activity-management.php"><i class="fas fa-chevron-left mr-1"></i> ACTIVITY MANAGEMENT</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>add new</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Activity Name*</label>
                                                        <input type="text" class="site-input" placeholder="Enter Activity Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Activity Description*</label>
                                                        <input type="text" class="site-input" placeholder="Enter Description">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Category*</label>
                                                    <div class="form-field">
                                                        <div class="form-field mb-0">
                                                            <select name="" id="" class="site-input">
                                                                <option value="">Hourly</option>
                                                                <option value="">Monthly</option>
                                                            </select>
                                                            <i class="fas fa-caret-down right-icon"></i>
                                                        </div>                                    
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Status*</label>
                                                    <div class="form-field">
                                                        <div class="form-field mb-0">
                                                            <select name="" id="" class="site-input">
                                                                <option value="">Active</option>
                                                                <option value="">Inactive</option>
                                                            </select>
                                                            <i class="fas fa-caret-down right-icon"></i>
                                                        </div>                                    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href="#" class="site-btn blue">Add</a>
                                            </div>
                                            <!-- <p class="form-heading pt-3 mb-3">Address Details</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State*</label>
                                                        <input type="text" class="site-input" placeholder="Abc">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City*</label>
                                                        <input type="email" class="site-input" placeholder="Abc">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label for="" class="site-label">Zip Code*</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <p class="select-pkg-text black-text">
                                                        <input type="radio" class="pkg-radio" id="test1" name="radio-group" checked="checked" value="ABC">
                                                        <label for="test1">Under My Supervision</label>
                                                    </p> 
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <p class="select-pkg-text black-text">
                                                        <input type="radio" class="pkg-radio" id="test2" name="radio-group" checked="" value="PQR">
                                                        <label for="test2">Under Manager's Supervision</label>
                                                    </p>
                                                </div>
                                                <div class="select-manager" style="display:none;">
                                                    <label for="" class="site-label pl-1">Select Managers*</label>
                                                    <select class="manager-dropdown">
                                                        <option selected="selected">Manager1</option>
                                                        <option>Abc</option>
                                                        <option>xyz</option>
                                                        <option>uio</option>
                                                    </select>
                                                </div>
                                                <div class="col-12">
                                                    <p class="black-text mb-0">
                                                        <input type="checkbox" id="stopover" name="radio-group">
                                                        <label for="stopover" class="bordered mb-0">Enable Registration Fee</label>
                                                    </p>
                                                </div>
                                            </div> -->
                                            <!-- <div class="text-center mt-3">
                                                <a data-toggle="modal" data-target=".registerAgent" class="site-btn blue">REGISTER</a>
                                            </div> -->
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
