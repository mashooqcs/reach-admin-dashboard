<?php 
  $pg='feedback';
  $title = "Feedback";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">feedback</h1>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills agent-pills" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Agents</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Managers</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>AGENT ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>EMAIL ADDRESS</th>
                                                            <th>DATE</th>
                                                            <th>ACTION</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>abc@xyz.com</td>
                                                            <td>Nov 20, 2020</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="feedback-details-agent.php"><i class="fa fa-eye"></i>VIEW</a> 
                                                                        <a class="dropdown-item" href="#"><i class="far fa-trash"></i>DELETE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>abc@xyz.com</td>
                                                            <td>Nov 20, 2020</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="feedback-details-agent.php"><i class="fa fa-eye"></i>VIEW</a> 
                                                                        <a class="dropdown-item" href="#"><i class="far fa-trash"></i>DELETE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>abc@xyz.com</td>
                                                            <td>Nov 20, 2020</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="feedback-details-agent.php"><i class="fa fa-eye"></i>VIEW</a> 
                                                                        <a class="dropdown-item" href="#"><i class="far fa-trash"></i>DELETE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>EMAIL ADDRESS</th>
                                                            <th>DATE</th>
                                                            <th>ACTION</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>abc@xyz.com</td>
                                                            <td>Nov 20, 2020</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="feedback-details-manager.php"><i class="fa fa-eye"></i>VIEW</a> 
                                                                        <a class="dropdown-item" href="#"><i class="far fa-trash"></i>DELETE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>abc@xyz.com</td>
                                                            <td>Nov 20, 2020</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="feedback-details-manager.php"><i class="fa fa-eye"></i>VIEW</a> 
                                                                        <a class="dropdown-item" href="#"><i class="far fa-trash"></i>DELETE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>abc@xyz.com</td>
                                                            <td>Nov 20, 2020</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="feedback-details-manager.php"><i class="fa fa-eye"></i>VIEW</a> 
                                                                        <a class="dropdown-item" href="#"><i class="far fa-trash"></i>DELETE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<?php include('footer.php') ?>

