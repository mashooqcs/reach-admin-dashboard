<?php 
  $pg='managers';
  $title = "Manager Edit Profile";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="manager-profile.php"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>edit profile</p>
                                            <p>MANAGER id:001</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <!-- <div class="text-md-right text-left">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p>May 2,2020</p>
                                                    <a href="#" class="site-btn sm-btn blue">EDIT</a>
                                            </div> -->
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        <input type="submit">
                                                    </form>
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="Lorem">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="Ipsum">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number*</label>
                                                        <input type="number" id="" class="site-input" placeholder="Enter Phone Number" value="232148">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="hamza@gmail" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="12sad23">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="USA">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State*</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="Texas">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City*</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="abc">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code*</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223" value="123">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center mt-3">
                                                <a href="manager-profile.php" class="site-btn blue">UPDATE</a>
                                            </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<?php include('footer.php') ?>
