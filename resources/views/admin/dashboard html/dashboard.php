<?php 
  $pg='dashboard';
  $title = "Dashboard";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
  <div class="content-wrapper">
    <div class="content-body"> 
      <!-- Basic form layout section start -->
      <section id="configuration">
        <div class="row">
          <div class="col-12">
            <div class="card ">
              <div class="card-content collapse show">
                <div class="card-dashboard">
                  <div class="row">
                    <div class="col-12">
                      <h1 class="yellow-text">Admin Dashboard</h1>
                    </div>
                    <div class="box right">
                      <div class="col-12">
                        <h3 class="user text-center black-text font-weight-bold">Quick Stats</h3>
                      </div>
                      <div class="row">
                        <div class="col-md-5 col-12 text-center">
                          <div class="c100 p70 big green"> <span>80%</span>
                            <div class="slice">
                              <div class="bar"></div>
                              <div class="fill"></div>
                            </div>
                          </div>
                          <h3 class="black-text font-weight-bold">Average Activities Completed/ Month</h3>
                        </div>
                        <div class="col-md-2 col-12">
                        </div>
                        <div class="col-md-5 col-12 text-center">
                          <div class="c100 p70 big green"> <span class="blue-text">80%</span>
                            <div class="slice">
                              <div class="bar blue-bar"></div>
                              <div class="fill blue-bar"></div>
                            </div>
                          </div>
                          <h3 class="black-text font-weight-bold">Average Activities Completed /Year</h3>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 d-flex w-100">
                      <div class="box right">												
                        <div class="dashboard-box">
                          <div class="custom-box">
                            <div class="orange">
                              <i class="fas fa-users"></i>
                            </div>
                            <div class="text">
                              <h3 class="user">REGISTERED</h3>
                              <h3 class="user">AGENTS</h3>
                            </div>
                          </div>
                          <div class="icon-card">
                            <span class="count-text">123</span>
                          </div>
                        </div>
										  </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 d-flex w-100">
                      <div class="box right">												
                        <div class="dashboard-box">
                          <div class="custom-box">
                            <div class="orange color">
                              <i class="fas fa-users"></i>
                            </div>
                            <div class="text">
                              <h3 class="user">REGISTERED</h3>
                              <h3 class="user">MANAGERS</h3>
                            </div>
                          </div>
                          <div class="icon-card">
                            <span class="count-text">123</span>
                          </div>
                        </div>
										  </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 d-flex w-100">
                      <div class="box right">												
                        <div class="dashboard-box">
                          <div class="custom-box">                        
                            <div class="orange">
                              <i class="fas fa-users"></i>
                            </div>
                            <div class="text">
                              <h3 class="user">MY</h3>
                              <h3 class="user">AGENTS</h3>
                            </div>
                          </div>
                          <div class="icon-card">
                            <span class="count-text">123</span>
                          </div>
                        </div>
										  </div>
                    </div>
                  </div>
                  <div class="bottom tickets">
                      <div class="col-12 text-center">
                        <h5 class="chart-heading text-capitalize">Activities Completed Per month</h5>
                      </div>
                      <div class="offset-md-9 col-lg-3 col-md-3 col-12 text-md-right text-center">
                        <div class="form-field">
                          <select class="site-input box-shadow" name="" id="" required="">
                              <option value="" selected disabled>Select Year</option>
                              <option value="">2020</option>
                              <option value="">2019</option>
                              <option value="">2018</option>
                          </select>
                          <i class="fa fa-chevron-down right-icon" aria-hidden="true"></i>
                        </div>
                      </div>
                      

                    <div class="chart-main position-relative">
                      <h4 class="side-heading">Activities</h4>
                      <div class="row">   
                        <div class="col-12">
                          <div id="column-chart" class="height-400 echart-container"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12 text-center">
                          <h3>Months</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                <!-- <div class="row ml-0 mr-0">
					        <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Line Basic Chart</div>
                            <div id="line-basic-chart"></div>
                        </div>
                    </div>
                  </div>
                </div> -->

					
						<!-- <div class="row ml-0 mr-0">
							<div class="col-12">
							<h2>Activity Log</h2>
							</div>
						</div> -->
						<!-- <div class="row">
							<div class="col-12">
                <div class="clearfix"></div>
                <div class="maain-tabble table-responsive dashboard">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>ACTIVITY</th>
                        <th>DATE</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>01</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </td>
                        <td>August-15-2019</td>
                      </tr>
                      <tr>
                        <td>02</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </td>
                        <td>August-15-2019</td>
                      </tr>
                      <tr>
                        <td>03</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </td>
                        <td>August-15-2019</td>
                      </tr>
                    </tbody>
                  </table>
                </div>   
							</div>
						</div> -->
				
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

<?php include('footer.php') ?>
