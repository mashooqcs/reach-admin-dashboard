<?php 
  $pg='agents';
  $title = "Inactive Agents - Assigned Activities";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="inactive-agents.php"><i class="fas fa-chevron-left mr-1"></i>INACTIVE AGENTS</a></h1>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills agent-pills" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Hourly</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Numeric</a>
                                        </li>
                                    </ul>
                                    <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <p class="form-heading pt-1 mb-0">001</p>
                                                    <p class="form-heading">John Smith</p>

                                                    <!-- <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        <input type="submit">
                                                    </form> -->
                                            </div>
                                    <p class="form-heading pl-1 mb-2">Assigned Activities</p>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                            <div class="row ml-0 mr-0 align-items-end">
                                                <div class="col-12">
                                                    <label  for="">Sort By:</label>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <label  for="">From:</label>
                                                    <input id="datepicker-1" class="site-input border" type="text" readonly>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <label  for="">To:</label>
                                                    <input id="datepicker-2" class="site-input border" type="text" readonly>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <a href="block-users.html" class="site-btn orange mb-1">APPLY/CLEAR</a>
                                                </div>
                                                <!-- <div class="col-xl-8 col-lg-6 col-12 text-md-right">
                                                    <div class="btn-with-filter">
                                                        <a href="block-users.html" class="site-btn green">BLOCK USERS</a>
                                                        <select name="" id="" class="filter-select mb-0">
                                                            <option value="">Filter by Status</option>
                                                            <option value="">Select 1</option>
                                                            <option value="">Select 2</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>ACTIVITY ID</th>
                                                            <th>NAME</th>
                                                            <th>MANAGER ID</th>
                                                            <th>ASSIGN DATE</th>
                                                            <th>START DATE</th>
                                                            <th>ACTUAL</th>
                                                            <th>GOAL</th>
                                                            <th>COMPLETE</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>001</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>10 hours</td>
                                                            <td>20 hours</td>
                                                            <td>50%</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" hreF="assigned-task-details-inactive.php"><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>001</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>May 2 ,2020</td>
                                                            <td>-</td>
                                                            <td>20 hours</td>
                                                            <td>-</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" hreF="assigned-task-details-inactive.php"><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="row ml-0 mr-0 align-items-end">
                                                <div class="col-12">
                                                    <label  for="">Sort By:</label>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <label  for="">From:</label>
                                                    <input id="datepicker-3" class="site-input border" type="text" readonly>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <label  for="">To:</label>
                                                    <input id="datepicker-4" class="site-input border" type="text" readonly>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <a href="block-users.html" class="site-btn orange mb-1">APPLY/CLEAR</a>
                                                </div>
                                                <!-- <div class="col-xl-8 col-lg-6 col-12 text-md-right">
                                                    <div class="btn-with-filter">
                                                        <a href="block-users.html" class="site-btn green">BLOCK USERS</a>
                                                        <select name="" id="" class="filter-select mb-0">
                                                            <option value="">Filter by Status</option>
                                                            <option value="">Select 1</option>
                                                            <option value="">Select 2</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>TASK ID</th>
                                                            <th>NAME</th>
                                                            <th>MANAGER ID</th>
                                                            <th>ASSIGN DATE</th>
                                                            <th>START DATE</th>
                                                            <th>ACTUAL</th>
                                                            <th>GOAL</th>
                                                            <th>COMPLETE</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>001</td>
                                                            <td>May 2,2020</td>
                                                            <td>May 2,2020</td>
                                                            <td>10 hours</td>
                                                            <td>20 hours</td>
                                                            <td>50%</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="assigned-task-details-inactive.php" ><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>001</td>
                                                            <td>May 2,2020</td>
                                                            <td>May 2,2020</td>
                                                            <td>10 hours</td>
                                                            <td>20 hours</td>
                                                            <td>50%</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="assigned-task-details-inactive.php" ><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>001</td>
                                                            <td>May 2,2020</td>
                                                            <td>May 2,2020</td>
                                                            <td>10 hours</td>
                                                            <td>20 hours</td>
                                                            <td>50%</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="assigned-tasks-details.php" ><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this agent's profile ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent xyz Profile has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>

