<?php 
  $title = "Login";
  include('header.php');
?>
  <section class="login-bg">
    <div class="container">
        <div class="login-card">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 col-12 pl-0 pr-0 login-left-col">
                </div>
                <div class="col-md-6 col-12 pl-0 pr-0 login-right-col">
                    <div class="login-right-content">
                        <h2 class="login-card-heading doctor">Login</h2>
                        <h5 class="login-subheading text-center black-text text-capitalize">Login in to your account</h5>
                        <div class="form-field">
                            <input type="email" class="site-input login left-icon" placeholder="Enter Email Address" name="" id="">
                            <i class="fa fa-envelope left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <i class="fa fa-lock left-icon" aria-hidden="true"></i>
                            <input type="password" class="site-input login both-icon enter-input" placeholder="Enter Password" name="" id="">
                            <i class="fa fa-eye-slash enter-icon right-icon" aria-hidden="true"></i>
                        </div>
                        <div class="below-input-div">
                            <p class="black-text mb-0">
                                <input type="checkbox" id="stopover" name="radio-group">
                                <label for="stopover" class="bordered mb-0">Remember Me</label>
                            </p>
                            <a href="#" class="forgot-link" data-toggle="modal" data-target="#pwdrecovery1">Forgot Password ?</a>
                        </div>
                        <a href="dashboard.php" class="login-btn d-block text-center text-uppercase">Login<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        <div class="col-12 text-center">
                            <a href="#" class="back-link d-inline-block mt-4"><i class="fa fa-arrow-left mr-2" aria-hidden="true"></i>back to Homepage</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


        <!-- Password Recovery Modal 1 -->
        <div class="modal fade" id="pwdrecovery1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered site-modal" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="site-modalheading">Password Recovery</h2>
                    <div class="form-field">
                        <i class="fa fa-envelope left-icon" aria-hidden="true"></i>
                        <input type="email" class="site-input login left-icon" placeholder="Enter Email Address" name="" id="">
                    </div>
                    <a class="login-btn mt-2" id="pwd1-btn">Continue</a>
                    <div class="text-center">
                        <a href="#" class="back-link d-inline-block mt-2" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Back to Login</a>
                    </div>
                </div>
            </div>
        </div>

      <!-- Password Recovery Modal 2 -->
      <div class="modal fade" id="pwdrecovery2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered site-modal" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h2 class="site-modalheading">Password Recovery</h2>
                  <div class="form-field mb-1">
                      <i class="fa fa-pen left-icon" aria-hidden="true"></i>
                      <input type="email" class="site-input login left-icon" placeholder="Enter Verification Code" name="" id="">
                  </div>
                  <a href="#" class="forgot-link d-block text-right mb-1">Didn't Receive Code? Send it Again</a>
                  <a href="#" class="login-btn mt-2" id="pwd2-btn">Continue</a>
                  <div class="text-center">
                      <a href="#" class="back-link d-inline-block mt-3" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Back to Login</a>
                  </div>
              </div>
          </div>
      </div>

        <!-- Password Recovery Modal 3 -->
        <div class="modal fade" id="pwdrecovery3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered site-modal" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h2 class="site-modalheading">Password Recovery</h2>
                  <div class="form-field">
                      <i class="fa fa-lock left-icon" aria-hidden="true"></i>
                      <input type="password" class="site-input login both-icon confirm-input" placeholder="Enter Password" name="" id="">
                      <i class="fa fa-eye-slash confirm-icon right-icon" aria-hidden="true"></i>
                  </div>
                  <div class="form-field">
                      <i class="fa fa-lock left-icon" aria-hidden="true"></i>
                      <input type="password" class="site-input login both-icon enter-input" placeholder="Retype Password" name="" id="">
                      <i class="fa fa-eye-slash enter-icon right-icon" aria-hidden="true"></i>
                  </div>
                  <a href="#" class="login-btn"  data-dismiss="modal" aria-label="Close">update</a>
                  <div class="text-center">
                      <a href="#" class="back-link d-inline-block mt-3" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Back to Login</a>
                  </div>
              </div>
            </div>
        </div>

<?php include('footer.php') ?>