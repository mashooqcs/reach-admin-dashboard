<?php 
  $title = "Register";
  include('header.php');
?>
  <section class="login-bg">
    <div class="container">
        <div class="login-card">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 col-12 pl-0 pr-0 login-left-col register">
                </div>
                <div class="col-md-6 col-12 pl-0 pr-0 login-right-col register">
                    <div class="login-right-content">
                        <h2 class="login-card-heading doctor">Therapist</h2>
                        <h6 class="login-subheading text-center black-text">Registration</h6>
                        <div class="form-field">
                            <input type="text" class="site-input login left-icon" placeholder="First Name*" name="" id="">
                            <i class="fas fa-user left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <input type="text" class="site-input login left-icon" placeholder="Last Name" name="" id="">
                            <i class="fas fa-user left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <input type="email" class="site-input login left-icon" placeholder="Email" name="" id="">
                            <i class="fa fa-envelope left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <i class="fas fa-lock left-icon" aria-hidden="true"></i>
                            <input type="password" class="site-input login both-icon enter-input" placeholder="Password" name="" id="">
                            <i class="fa fa-eye-slash enter-icon right-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <i class="fas fa-lock left-icon" aria-hidden="true"></i>
                            <input type="password" class="site-input login both-icon enter-input" placeholder="Confirm Password" name="" id="">
                            <i class="fa fa-eye-slash enter-icon right-icon" aria-hidden="true"></i>
                        </div>
                        <a href="login.php" class="login-btn d-block text-center text-uppercase">Register<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        <div class="col-12 text-center">
                            <a href="login.php" class="back-link d-inline-block mt-4">Already have an account? Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php') ?>