<?php 
$title = 'Activity management';
$pg = 'activity';
?>
@extends('admin.layout.app')
@section('title', __('Activiy Management'))
@section('content')
<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                    @if(Session::has('success'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                    @elseif(Session::has('error'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                    @endif
                                    @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            <h1 class="mb-2">ACTIVITY MANAGEMENT</h1>
                                        </div>
                                    </div>
                                    <div class="text-md-right text-center">
                                        <a href="{{route('createActivityManagement')}}" class="site-btn blue mb-1">ADD NEW</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="maain-tabble table-responsive">
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>S.NO</th>
                                                    <th>ACTIVITY ID</th>
                                                    <th>NAME</th>
                                                    <th>DESCRIPTION</th>
                                                    <th>CATEGORY</th>
                                                    <th>STATUS</th>
                                                    <th>ACTIONS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($data))
                                            <?php $i=1;?>
                                                @foreach($data as $activity)
                                                    <tr class = "activity-tr">
                                                        <td>{{$i}}</td>
                                                        <td class = "activity-id">{{$activity->activity_id}}</td>
                                                        <td class = "activity-name">{{$activity->activity_name}}</td>
                                                        <td class = "activity-description">{{$activity->activity_description}}</td>
                                                        <td class = "activity-category-name">{{$activity->category_name}}</td>
                                                        <td>
                                                            <div class="form-field mb-0">
                                                                        <select name="" class="table-select select-modal">
                                                                            <option <?php if($activity->activity_status =='1'){ echo 'selected';} ?>value="1">Active</option>
                                                                            <option <?php if($activity->activity_status =='0'){ echo 'selected';} ?> value="0">Inactive</option>
                                                                        </select>
                                                                        <i class="fas fa-caret-down right-icon"></i>
                                                                    </div>
                                                            <!-- Active -->
                                                        </td>
                                                        <td>
                                                            <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                <button type="button" class="btn btn-drop-table btn-sm"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false"> <i
                                                                        class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu custom-dropdown">
                                                                    <a class="dropdown-item activity-edit" data-toggle="modal" data-target=".activityModal">
                                                                        <i class="fa fa-edit"></i>EDIT 
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++;?>
                                                @endforeach
                                            @endif            

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Edit Activity Modal -->
<div class="modal fade activityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <p class="modal-heading text-center">Edit Task</p>
            <div class="row">
            <form method = "POST" action = "{{route('updateActivity')}}" style="width: 100%;">
            {{csrf_field()}}
                <div class="col-md-12 col-12">
                    <div class="form-field">
                        <label for="" class="site-label">Activity Name*</label>
                        <input type="text" name = "activity-name" class="site-input activity-name-input" placeholder="Enter Activity Name" value = "">
                        <input type="hidden" name = "activity-id" class = "activity-id-input" value = "">

                    </div>
                </div>
                <div class="col-md-12 col-12">
                    <div class="form-field">
                        <label for="" class="site-label">Activity Description*</label>
                        <input type="text" name = "activity-description" class="site-input activity-description-input" placeholder="Enter Description">
                    </div>
                </div>
                <div class="col-md-12 col-12">
                    <label for="" class="site-label">Category*</label>
                    <div class="form-field">
                           <select name="category-id" class="site-input">
                            <option selected value=''>Select</option>
                                @if(count($cats))
                                    @foreach($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                 @endif               
                            </select>
                            <i class="fas fa-caret-down right-icon"></i>
                        
                    </div>
                </div>
                <div class="col-md-12 col-12">
                    <label for="" class="site-label">Status*</label>
                    <div class="form-field">
                            <select name="status" id="select-modal" class="site-input">
                                <option value="0">Inactive</option>
                                <option selected value="1">Active</option>
                            </select>
                            <i class="fas fa-caret-down right-icon"></i>
                        
                    </div>
                </div>
            </div>
            <div class="text-center mt-2">
            <button type = "submit" class="site-btn blue"> Update </button>
            </div>
           
        </div>

    </div>
</div>

<!-- Inactive Activity Modal -->
<div class="modal fade inActiveActivity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this activity ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesInactiveActivity">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Confirm Inactive Activity Confirmation -->
<div class="modal fade confirmInactiveActivity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Activity xyz has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Active Activity Modal -->
<div class="modal fade activeActivity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to active this activity ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesActiveActivity">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Confirm Active Activity Confirmation -->
<div class="modal fade confirmActiveActivity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Activity xyz has been activated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection