<?php 
$title = 'Payment Detail';
$pg = 'payment';
?>
@extends('admin.layout.app')
@section('title', __('Inactive managers'))
@section('content')

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-lg-7 col-12">
                                            <h1 class="">Payment Details</h1>
                                        </div>
                                        <div class="col-lg-5 col-12">
                                            <div class="directory-page">
                                                <ul class="site-ul directory-ul">
                                                    <li class="directory-li">
                                                        <a href="payment-log.php">Payment Log</a>
                                                    </li>
                                                    <li class="directory-li">
                                                        <a href="#">Payment Detail</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="form-field">
                                                <label for="" class="site-label">User Email:</label>
                                                <input type="email" class="site-input" name="" id="" placeholder="Enter Email" value="testing@email.com" readonly>
                                            </div>
                                            <div class="form-field">
                                                <label for="" class="site-label">Session Conducted on:</label>
                                                <input type="text" class="site-input" name="" id="" placeholder="Enter Date" value="May, 2 2020" readonly>
                                            </div>
                                            <div class="form-field">
                                                <label for="" class="site-label">Session Type:</label>
                                                <input type="text" class="site-input" name="" id="" placeholder="Enter Session Type" value="Video Call" readonly>
                                            </div>
                                            <div class="form-field">
                                                <label for="" class="site-label">Session Fee:</label>
                                                <input type="text" class="site-input" name="" id="" placeholder="Enter Session Fee" value="$60" readonly>
                                            </div>
                                            <div class="form-field">
                                                <label for="" class="site-label">My Earning:</label>
                                                <input type="text" class="site-input" name="" id="" placeholder="My Earning" value="$60" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12 text-md-right text-left">
                                            <p class="site-text lg-text black-text">Status: Paid</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection