<?php 
$title = 'Discussion';
$pg = 'agents';
?>
@extends('admin.layout.app')
@section('title', __('Discussion board'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>
<?php $helper = app('App\Services\HelperService'); ?>


<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('agents.index')}}"><i class="fas fa-chevron-left mr-1"></i> AGENTS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>discussion board</p>
                                        </div>
                                        <div class="card-area board">
                                            <div class="profile-picture-div">
                                            @if($data['userData']->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data['userData']->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif 
                                                    <p class="form-heading pt-1 mb-0">{{$data['userData']->id}}</p>
                                                    <p class="form-heading">{{$data['userData']->first_name}}{{' '}}{{$data['userData']->last_name}}</p>

                                    
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            @if(Session::has('success'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                                @elseif(Session::has('error'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                                @endif
                                            
                                                @if ($errors->any())
                                                                    <div class="alert alert-danger">
                                                                        <ul>
                                                                            @foreach ($errors->all() as $error)
                                                                                <li>{{ $error }}</li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                @endif
                                            <form action = "{{route('addDiscussion')}}" method = "POST">
                                            {{csrf_field()}}
                                            <div class="form-field">
                                                <textarea class="site-input" name="message" id="" cols="30" rows="10" placeholder="Write Something Here"></textarea>
                                                <input type = "hidden" name = "user_id" value = "{{$data['userData']->id}}">

                                                <button type = "submit" class="site-btn blue mt-1">SEND</button>
                                            </div>
                                            </form>
                                            @if(count($data['discussionData']))
                                                @foreach($data['discussionData'] as $discussion)
                                                <div class="message-thread-div">
                                                    @if($discussion->sender_type=='agent')
                                                   
                                                            <div class="message-thread">
                                                                <span class="letter blue">T</span>
                                                                <div class="message-body">
                                                                    <p class="site-text black-text">Agent</p>
                                                                    <div class="d-flex align-items-center">
                                                                        <p><?php echo  $notificationService::timeFormat($discussion->created_at);?><p><?php echo  $notificationService::dateFormat($discussion->created_at);?></p>
                                                                    </div>
                                                                    <p class="site-text">{{$discussion->message}}</p>
                                                                </div>
                                                            </div>
                                          
                                                       
                                                        @else
                                                        <div class="message-thread admin-thread-div">
                                                            <div class="message-thread">
                                                                <span class="letter orange">A</span>
                                                                <div class="message-body">
                                                                    <p class="site-text black-text">Admin</p>
                                                                    <div class="d-flex align-items-center">
                                                                        <p><?php echo  $notificationService::dateFormat($discussion->created_at);?>  <?php echo  $notificationService::timeFormat($discussion->created_at);?></p>
                                                                    </div>
                                                                    <p class="site-text">{{$discussion->message}}</p> 
                                                                </div>
                                                            </div>
                                                       
                                                    </div>
                                                    @endif()
                                                 @endforeach()
                                                 @else
                                                 <p class="form-heading pt-1 mb-2">No Message Found</p>
                                                 @endif()   

                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection