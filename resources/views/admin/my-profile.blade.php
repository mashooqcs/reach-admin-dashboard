@extends('admin.layout.app')
@section('title', __('My profile'))
@section('content')

<div class="app-content content dashboard">
  <div class="content-wrapper">
    <div class="content-body"> 
      <!-- Basic form layout section start -->
      <section id="configuration">
        <div class="row">
          <div class="col-12">
            <div class="card ">
              <div class="card-content collapse show">
                <div class="card-dashboard">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="">my profile</h1>
                        </div>
                    </div>
                    <div class="profile-picture-div">
                        <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                        <label for="picture">
                                    <i class="fas fa-camera profile-pic-icon"></i>
                                </label>
                                <form style="display: none;">
                                <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                <input type="submit">
                            </form>
                    </div>
                    <div class="manage-profile-div">
                        <h3 class="black-text text-uppercase font-weight-bold">DOCTOR A</h3>
                        <a href="manage-profile.php" class="site-btn green">MANAGE PROFILE</a>
                        <h3 class="black-text text-uppercase font-weight-bold">
                            Nothing to show please<br>
                            manage your profile and verify<br>
                            your account
                        </h3>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

@endsection