<?php 
$title = 'Inactive managers';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Inactive managers'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>
<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('managers.index')}}"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    <p class="form-heading pl-1 mb-2">Inactive Managers</p>

                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FEE</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data))
                                                    @foreach($data as $manager)
                                                        <tr class ="manager-tr">
                                                            <td class="manager-id">{{$manager->id}}</td>
                                                            <td>{{$manager->id}}</td>
                                                            <td>{{$manager->first_name}}{{' '}}{{$manager->last_name}}</td>
                                                            <td><?php echo $dtime =  $notificationService::dateTimeFormat($manager->created_at);?></td>

                                                            <td></td>
                    
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <!-- <a class="dropdown-item deactivate-manager-status" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>ACTIVE </a>  -->
                                                                        <a class="dropdown-item" href="{{route('managerProfile', $manager->id)}}"><i class="fa fa-eye"></i>VIEW PROFILE</a> 
                                                                        <!-- <a class="dropdown-item" href="view-assigned-agents2.php"><i class="fas fa-clipboard-list"></i>VIEW ASSIGNED AGENTS </a>  -->
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                     @endforeach
                                                     @endif
                                                       
                                                    </tbody>
                                                </table>
                                            </div> 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to active this manager's profile ?</p>
                <div class="modal-btn-div">
                <input type = "hidden" name ='manager-id-input' class = "manager-id-input">
                    <a class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Manager Profile has been activated</p>
                <div class="modal-btn-div">
                <form method = "POST" action  ="{{route('updateUserStatus')}}">
                {{csrf_field()}}
                    <input type = "hidden" name ='manager-id-input' class = "manager-id-input">
                    <input type = "hidden" name ='type' value = "active">
                    <button class="site-btn blue" type = "submit">GOT IT</button>
                </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>

@endsection