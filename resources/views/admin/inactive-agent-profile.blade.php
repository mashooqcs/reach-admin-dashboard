<?php 
$title = 'Inactive Agent Profile';
$pg = 'agents';
?>
@extends('admin.layout.app')
@section('title', __('Inactive agent profile'))
@section('content')

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="inactive-agents.php"><i class="fas fa-chevron-left mr-1"></i> INACTIVE AGENTS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>view profile</p>
                                            <p>agent id:001</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <div class="text-info-area">
                                                <p class="form-heading mb-0">Registration Date :</p>
                                                <p>May 2,2020</p>
                                                <a href="agent-edit-profile.php" class="site-btn sm-btn blue">EDIT</a>
                                            </div>
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="Lorem" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="Ipsum" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number</label>
                                                        <input type="number" id="" class="site-input" placeholder="Enter Phone Number" value="232148" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="hamza@gmail" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="12sad23" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="USA" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="Texas" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="abc" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Zip Code</label>
                                                        <input type="number" class="site-input enter-input" placeholder="1223" value="123" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-heading">
                                            <p>coach details</p>
                                        </div>

                                        <div class="card-area">
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <!-- <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/" id="picture">
                                                        <input type="submit">
                                                    </form> -->
                                            </div>
                                            <p class="form-heading mb-0">Manager Id: 001</p>
                                            <p class="form-heading">Manager Name</p>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection