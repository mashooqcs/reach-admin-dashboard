@extends('admin.layout.app')
@section('title', __('User profile'))
@section('content')

<?php  $authUser = Auth()->user(); ?>
<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('myProfile')}}"><i class="fas fa-chevron-left mr-1"></i> PROFILE</a></h1>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>view profile</p>
                                        </div>
                                        <div class="card-area">
                                            <div class="text-center">
                                                <a class="site-btn sm-btn blue mb-2" data-toggle="modal" data-target=".changePwd">CHANGE PASSWORD</a>
                                                @if(Session::has('success'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success') }}</p>
                                                @elseif(Session::has('error'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                                @endif

                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <form method = 'POST' action = "{{route('updateProfile')}}" enctype='multipart/form-data'>
                                            {{csrf_field()}}
                                            <div class="profile-picture-div">
                                            @if($authUser->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$authUser->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif 

                                                    <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                       
                                                        <input style="display: none;" type="file" name="profile_image" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                   
                                            </div>
                                            <p class="form-heading pt-1 mb-3">Personal Information</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" value="{{$authUser->first_name}}" name = "first_name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input" value="{{$authUser->last_name}}" name = "last_name" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number*</label>
                                                        <input type="number" id="" class="site-input" value="{{($authUser->phone_number)?$authUser->phone_number:''}}" name = "phone_number">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" value="{{($authUser->email)?$authUser->email:''}}" name = "email" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="form-heading pt-1 mb-3">Address Details</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" value="{{($authUser->address)?$authUser->address:''}}" name = "address" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country*</label>
                                                        <input type="text" class="site-input" value="{{($authUser->country)?$authUser->country:''}}" name = "counry">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State*</label>
                                                        <input type="text" class="site-input" value="{{($authUser->state)?$authUser->state:''}}" name = "state">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City*</label>
                                                        <input type="text" class="site-input" value="{{($authUser->city)?$authUser->city:''}}" name = "city">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code*</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" value="{{($authUser->zip_code)?$authUser->zip_code:''}}" name = "zip_code" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                            <input type = 'submit'class="site-btn blue"  value= "Update"  >
                                            </div>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade changePwd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <form method = 'POST' action = "{{route('passwordChange')}}" enctype='multipart/form-data'>
            {{csrf_field()}}

                
                <p class="modal-heading text-center">Change Password</p>
                <label for="" class="site-label">Current Password*</label>
                <div class="form-field">
                    <input type="password" class="site-input right-icon enter-input inputpassword inputpassword1 " placeholder="Enter Password" name="old_password" >
                    <i class="fa fa-eye-slash  right-icon passeye1" aria-hidden="true"></i>
                </div>
                <label for="" class="site-label">New Password*</label>
                <div class="form-field">
                    <input type="password" class="site-input right-icon current-input inputpassword inputpassword2 " placeholder="Enter New Password" name="new_password" >
                    <i class="fa fa-eye-slash  right-icon passeye2" aria-hidden="true"></i>
                </div>
                <label for="" class="site-label">Confirm Password*</label>
                <div class="form-field">
                    <input type="password" class="site-input right-icon confirm-input inputpassword inputpassword3" placeholder="Confirm Password" name="confirm_password">
                    <i class="fa fa-eye-slash right-icon passeye3" aria-hidden="true"></i>
                </div>
                <div class="modal-btn-div">
                <input type = 'submit' class="site-btn blue"  value= "Update" >
                </div>
                </form>
       
        </div>
    </div>
</div>

@endsection