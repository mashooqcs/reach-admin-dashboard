<?php 
$title = 'Rejected request details';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Reject request details'))
@section('content')


<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('managersRequests')}}"><i class="fas fa-chevron-left mr-1"></i>REJECTED PROFILE</a></h1>
                                        </div>
                                    </div>
                                    <div class="profile-picture-div">
                                    @if($data->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif                                      </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>Personal Information</p>
                                            <p>Verification Request Id: {{$data->id}}</p>
                                        </div>
                                      
                                        <div class="card-area">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">First Name</label>
                                                            <p class="site-text">{{$data->first_name}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Last Name</label>
                                                            <p class="site-text">{{$data->last_name}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Phone Number</label>
                                                            <p class="site-text">{{$data->phone_number}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Email Address</label>
                                                            <p class="site-text">{{$data->email}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="card-heading">
                                            <p>Address Details</p>
                                        </div>
                                            <div class="card-area">
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Address</label>
                                                            <p class="site-text">{{$data->address}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Country</label>
                                                            <p class="site-text">{{$data->country}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">State</label>
                                                            <p class="site-text">{{$data->state}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">City</label>
                                                            <p class="site-text">{{$data->city}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Zip Code</label>
                                                            <p class="site-text">{{$data->zip_code}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-field">
                                                    <label for="" class="site-label font-weight-bold">Rejection Reason</label>
                                                    <p class="site-text">{{$data->rejection_reason}} </p>
                                                </div>
                                            </div>
                                            </div>           
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Manager Modal -->
<div class="modal fade registerManager" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this Manager?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange yesmanager">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Manager Confirmation -->
<div class="modal fade yesregisterManager" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Manager has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Reason -->
<div class="modal fade rejectionReason" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
            </div>
                <div class="form-field">
                    <label for="" class="site-label font-weight-bold">Enter Rejection Reason</label>
                    <textarea name="" id="" cols="30" rows="10" class="site-input border" placeholder="Enter Rejection Reason"></textarea>
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn orange" data-dismiss="modal" aria-label="Close">SUBMIT</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">CANCEL</a>
                </div>

        </div>
    </div>
</div>
@endsection
