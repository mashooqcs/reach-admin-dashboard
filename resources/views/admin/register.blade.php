<?php 
    $pg = 'Regiser';
    $title = "Register";
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> R.E.A.C.H | <?php echo ((isset($title))?$title:'Dashboard'); ?> </title>
    <link rel="shortcut icon" href="{{asset('assets/admin/images/favicon.png')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/intlTelInput.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/CustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
  <!-- END Custom CSS-->
</head>

<body class="<?php if ($title=="Login" || $title=="Register" || $title=="Error404" )  {echo "pt-0";} else {echo "";}?>" data-open="click" data-menu="vertical-menu" data-col="2-columns">

  <section class="login-bg">
    <div class="container">
        <div class="login-card">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 col-12 pl-0 pr-0 login-left-col register">
                </div>
                <div class="col-md-6 col-12 pl-0 pr-0 login-right-col register">
                    <div class="login-right-content">
                         
                        <h2 class="login-card-heading doctor">Therapist</h2>
                        <h6 class="login-subheading text-center black-text">Registration</h6>
                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <form method = 'POST' action = "{{route('registerAdmin')}}" enctype='multipart/form-data'>
                        {{csrf_field()}}

                        <div class="form-field">
                            <input type="text" class="site-input login left-icon" required = 'required' placeholder="First Name" name="first_name" id="">
                            <i class="fas fa-user left-icon" aria-hidden="true"></i>
                        </div>
                 
                        <div class="form-field">
                            <input type="text" class="site-input login left-icon" required = 'required' placeholder="Last Name" name="last_name" id="">
                            <i class="fas fa-user left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <input type="email" class="site-input login left-icon" required = 'required' placeholder="Email" name="email" id="">
                            <i class="fa fa-envelope left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <i class="fas fa-lock left-icon" aria-hidden="true"></i>
                            <input type="password" class="site-input login both-icon enter-input inputpassword"  required = 'required' placeholder="Password" name="password" id="">
                            <i class="fa fa-eye-slash enter-icon right-icon passeye" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <i class="fas fa-lock left-icon" aria-hidden="true"></i>
                            <input type="password" class="site-input login both-icon enter-input inputpassword" required = 'required' placeholder="Confirm Password" name="confirm_password" id="">
                            <i class="fa fa-eye-slash enter-icon right-icon passeye" aria-hidden="true"></i>
                        </div>
                        <button type = "submit" class="login-btn d-block text-center text-uppercase btn-block" value  = "Register">Register</button>
                        <div class="col-12 text-center">
                        
                            <a href="login" class="back-link d-inline-block mt-4">Already have an account? Login</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- BEGIN VENDOR JS--> 
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/admin/app-assets/vendors/js/vendors.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/vendors/js/charts/chart.min.js')}}"></script> 
<script src="{{asset('assets/admin/app-assets/js/core/app-menu.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/core/app.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/customizer.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/chart.js')}}"></script> 
<script src="{{asset('assets/admin/assets/js/jquery.repeater.min.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/form-repeater.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/chart.js')}}" ></script> 
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="{{asset('assets/admin/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{asset('assets/admin/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/vendors/js/charts/echarts/echarts.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/vendors/js/extensions/moment.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/charts/chartjs/line/line.js')}}"></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/charts/chartjs/line/line-area.js')}}"></script> 
<!-- <script src="app-assets/js/scripts/charts/chartjs/line/line-logarithmic.js"></script> -->
<!-- <script src="app-assets/js/scripts/charts/chartjs/line/line-multi-axis.js"></script> -->

<script src="{{asset('assets/admin/app-assets/js/scripts/modal/components-modal.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/intlTelInput.js')}}"></script> 
<script src="{{asset('assets/admin/assets/js/function.js')}}" ></script> 

<!-- BEGIN VENDOR JS--> 
<!-- BEGIN VENDOR JS--> 
<!-- BEGIN PAGE VENDOR JS--> 
<!-- END PAGE VENDOR JS--> 
<!-- BEGIN STACK JS--> 
    <script>
    $(document).ready(function(){
        $('.passeye').click(function(){
            let elem  = $(".inputpassword")
            if ('password' == $(elem).attr('type')) {
                $(elem).prop('type', 'text');
            } else {
                $(elem).prop('type', 'password');    
            }
        });

        
            
    });
    </script>
</body>
</html>