<?php 
$title = 'Managers management';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Register managers'))
@section('content')


<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('managers.index')}}"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>Register Manager</p> 
                                        </div>
                                        <div class="card-area">
                                            <div class="profile-picture-div">
                                            <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                    <form method = "POST" action = "{{route('addManager')}}" enctype='multipart/form-data'>
                                            {{csrf_field()}}
                                                        <input style="display: none;" type="file" name="profile_image" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                            </div>
                                            <p class="form-heading orange-text pt-1 mb-3">Personal Information</p>
                                            @if(Session::has('success'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success') }}</p>
                                    @elseif(Session::has('error'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                    @endif

                                    @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input required type="text" class="site-input" placeholder="Enter First Name" name = "first_name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input required type="text" class="site-input" placeholder="Enter Last Name" name = "last_name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number</label>
                                                        <input type="number" id="" class="site-input" placeholder="Enter Phone Number" name = "phone_number">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input required type="email" class="site-input" placeholder="Enter Email" name = "email">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Password*</label>
                                                    <div class="form-field">
                                                        <input required type="password" class="site-input confirm-input inputpassword" placeholder="enter password" name = "password">
                                                        <i class="fas fa-eye-slash right-icon confirm-icon passeye" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Confirm Password*</label>
                                                    <div class="form-field">
                                                        <input required type="password" class="site-input current-input inputpassword1" placeholder="enter password" name = "confirm_password">
                                                        <i class="fas fa-eye-slash right-icon passeye1" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="form-heading orange-text pt-3 mb-3">Address Details</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" placeholder="Enter address"  name = "address">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" placeholder="enter country" name = "country">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" placeholder="enter state" name = "state">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="text" class="site-input" placeholder="enter city" name = "city">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label for="" class="site-label">Zip Code</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="enter zip code" name = "zip_code">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <p class="black-text mb-0">
                                                        <input type="checkbox" id="stopover" name="fee_enabled">
                                                        <label for="stopover" class="bordered mb-0">Enable Registration Fee</label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="text-center mt-3">
                                            <button class="site-btn blue" type = "submit" >REGISTER</button> 

                                            </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection