<?php 
$title = 'Category management';
$pg = 'category';
?>
<?php $helper = app('App\Services\HelperService'); ?>
@extends('admin.layout.app')
@section('title', __('Category management'))
@section('content')
<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                    @if(Session::has('success'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                    @elseif(Session::has('error'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                    @endif
                                    @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            <h1 class="mb-2">CATEGORY MANAGEMENT</h1>
                                        </div>
                                    </div>
                                    <div class="text-md-right text-center">
                                        <a data-toggle="modal" data-target=".addCategoryModal" class="site-btn blue mb-1">ADD NEW</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="maain-tabble table-responsive">
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>S.NO</th>
                                                    <th>CATEGORY ID</th>
                                                    <th>CATEGORY NAME</th>
                                                    <th>NO. OF TASK</th>
                                                    <th>ACTIONS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($data))
                                            @foreach($data as $cat)
                                                <tr class="cat-tr">
                                                    <td>{{$cat->id}}</td>
                                                    <td class = "cat-id">{{$cat->id}}</td>
                                                    <td class = "cat-name">{{$cat->name}}</td>
                                                    <td><?php echo $helper::getActivityCount($cat->id)?></td>
                                                    <td>
                                                        <div class="btn-group custom-dropdown ml-2 mb-1">
                                                            <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                                <i class="fa fa-ellipsis-v"></i>
                                                            </button>
                                                            <div class="dropdown-menu custom-dropdown">
                                                                <a class="dropdown-item catEdit" data-toggle="modal" data-target=".editCategoryModal">
                                                                    <i class="fa fa-edit"></i>EDIT 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                             @endforeach
                                             @endif   
                                                

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>



<!-- Edit Category Modal -->
<div class="modal fade editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <p class="modal-heading text-center">Edit Category</p>
            <div class="row">
                <div class="col-12">
                    <div class="form-field">
                    <form method= "POST" action = "{{route('updateCategory')}}">
                    {{csrf_field()}}
                        <label for="" class="site-label">Category Name*</label>
                        <input type="text" class="site-input edit-cat-input" name = "category_name" placeholder="Enter Category Name" value="">
                        <input type="hidden" class="edit-cat-id-input" name = "category_id" value="">
                   
                    </div>
                </div>
            </div>
            <div class="text-center mt-2">
            <button class="site-btn blue" type = "submit">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Category Modal -->
<div class="modal fade addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <p class="modal-heading text-center">Add Category</p>
            <div class="row">
                <div class="col-12">
                    <div class="form-field">
                    <form  method = 'POST' action= "{{route('addCategory')}}">
                    {{csrf_field()}}
                        <label for="" class="site-label">Category Name*</label>
                        <input type="text" name = "cat_name" class="site-input" placeholder="Enter Category Name">
                    
                    </div>
                </div>
            </div>
            <div class="text-center mt-2">
            <button type = "submit"  class="site-btn blue">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection
