<?php 
$title = 'Settings';
$pg = 'settings';
?>
@extends('admin.layout.app')
@section('title', __('Edit settings'))
@section('content')

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Settings</h1>
                                        </div>
                                        
                                    </div>
                                    @if(Session::has('success'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success') }}</p>
                                                @elseif(Session::has('error'))
                                                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                                @endif
                                    <div class="custom-card">  
                                        <div class="card-area full">
                                            <p class="form-heading mb-2">Agent</p>
                                            <form method = "POST" action = "{{route('updateSettings')}}" >
                                            {{csrf_field()}}
                                            <div class="row form-field align-items-end">
                                                <div class="col-md-4 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <input type="text" name = 'agent_fee' class="site-input" value="<?php if($data){ echo $data->agent_fee;} ?>" placeholder="Enter Fee">
                                                </div>
                                                <div class="col-md-8 col-12 text-md-right text-left">
                                                <button type = "submit" class="site-btn blue mt-md-0 mt-1">Update</button>

                                                </div>
                                            </div>
                                            <p class="form-heading mb-2">Manager</p>
                                            <div class="row form-field align-items-end">
                                                <div class="col-md-4 col-12">
                                                    <p class="mb-0">Registration Fee</p>
                                                    <input type="text" name = 'manager_fee' class="site-input" value="<?php if($data){ echo $data->manager_fee;} ?>" placeholder="Enter Fee">
                                                </div>
                                                <div class="col-md-8 col-12 text-md-right text-left">
                                                <button type = "submit" class="site-btn blue mt-md-0 mt-1">Update</button>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection