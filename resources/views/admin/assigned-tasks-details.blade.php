<?php 
$title = 'Assigned task details';
$pg = 'agents';
?>
@extends('admin.layout.app')
@section('title', __('Assigned tasks detail'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('agents.index')}}"><i class="fas fa-chevron-left mr-1"></i> AGENTS</a></h1>
                                        </div>
                                    </div>
                                    <div class="profile-picture-div pb-2">
                                    @if($data['userData']->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data['userData']->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif                                         <p class="form-heading pt-1 mb-0">{{$data['userData']->id}}</p>
                                        <p class="form-heading">{{$data['userData']->first_name}}{{' '}}{{$data['userData']->last_name}}</p>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>activity details</p>
                                            <p>activity id: {{$data['activityData']->user_activity_id}}</p>
                                        </div>
                                        <div class="card-area">
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Activity Name </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">{{$data['activityData']->name}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Activity Category</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Description</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">
                                                           
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Assigned Date</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text"><?php echo $notificationService::dateTimeFormat($data['activityData']->created_at);?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Start Date</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text"><?php echo $notificationService::dateTimeFormat($data['activityData']->created_at);?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-info">
                                                <p class="site-text green-text">Actual : 10 Hours </p>
                                                <p class="site-text green-text">Goal : 20 Hours </p>
                                                <p class="site-text green-text">Complete : 50% </p>
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>

                                        <div class="card-heading">
                                            <p>Progress Breakdown</p>
                                        </div>
                                        <div class="card-area">
                                
                                            <div class="clearfix"></div>
                                                    <div class="maain-tabble table-responsive">
                                                        <table class="table table-striped table-bordered zero-configuration">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.NO</th>
                                                                    <th>DATE</th>
                                                                    <th>DAY</th>
                                                                    <th>DATA ADDED</th>
                                                                    <th>Actual</th>
                                                                    <th>Goal</th>
                                                                    <th>Complete</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                

                                                            </tbody>
                                                        </table>
                                                    </div> 
                                                        
                                        </div>        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection