<?php 
$title = 'Manager Edit Profile';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Manager edit profile'))
@section('content')


<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('managerProfile', $data->id)}}"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    
                                    @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>edit profile</p>
                                            <p>MANAGER id:{{$data->id}}</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <!-- <div class="text-md-right text-left">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p>May 2,2020</p>
                                                    <a href="#" class="site-btn sm-btn blue">EDIT</a>
                                            </div> -->
                                            <form method = "POST" action = "{{route('managerUpdateProfile')}}" enctype='multipart/form-data'>
                                            {{csrf_field()}}
                                             <input type = "hidden" name = "user_id" value = "{{$data->id}}">       
                                            <div class="profile-picture-div">
                                                    @if($data->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif

                                                    <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <input style="display: none;"  type="file" name="profile_image" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        
                                                   
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" name = "first_name" value="{{$data->first_name}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" name = "last_name" value="{{$data->last_name}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number</label>
                                                        <input type="number" id="" class="site-input" placeholder="Enter Phone Number" name = "phone_number" value="{{($data->phone_number)?$data->phone_number:''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="{{($data->email)?$data->email:''}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="{{($data->address)?$data->address:''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="{{($data->country)?$data->country:''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="{{($data->state)?$data->state:''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="{{($data->city)?$data->city:''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223" value="{{($data->zip_code)?$data->zip_code:''}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center mt-3">
                                            <button type = "submit" class="site-btn blue">UPDATE</button>
                                            </div>
                                            </form>                       
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


@endsection