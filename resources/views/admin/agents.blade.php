<?php 
$title = 'Agents';
$pg = 'agents';
?>
@extends('admin.layout.app')
@section('title', __('Agents'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>
<?php $helper = app('App\Services\HelperService'); ?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                @if(Session::has('msg'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
                                @elseif(Session::has('error'))
                                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                @endif
                               
                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                  @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">agents</h1>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills agent-pills" id="pills-tab" role="tablist">
                                       
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Manager's Agents</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">My Agents</a>
                                        </li>

                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                            <div class="text-md-right text-left mb-2">
                                                <a href="{{ url('admin/inactive-agents') }}" class="site-btn lg-btn blue">INACTIVE AGENTS</a>
                                                <a href="{{ url('admin/register-agents') }}" class="site-btn lg-btn orange text-uppercase ml-2">register AGENTS</a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>AGENT ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FEE</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $i = 1; ?>
                                                    @if(count($data['managersAgent']))
                                                    @foreach($data['managersAgent'] as $agent)
                                                        <tr class="agent_tr">
                                                            <td>{{ $i++ }}</td>
                                                            <td class="agent_id">{{ $agent->user_id }}</td>
                                                            <td>{{ $agent->first_name.' '.$agent->last_name }}</td>
                                                            <td><?php echo $dtime =  $notificationService::dateTimeFormat($agent->created_at);?></td>                                                        
                                                            <td>{{ $agent->fee_enabled }}</td>
                                                            <td>{{ ($agent->manager_id)?$agent->manager_id:"" }}</td>
                                                            <td>{{($agent->manager_id)?$helper::getManagerName($agent->manager_id):""}}</td>                                                        
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item inactive" href="" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                        <a class="dropdown-item assign-task" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a href="{{route('agentProfile', $agent->user_id)}}" class="dropdown-item" ><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="{{route('viewAssignedActivities', $agent->user_id)}}"><i class="fas fa-clipboard-list"></i>View Assigned Activities</a> 
                                                                        <a class="dropdown-item" href="{{route('viewMonthlyProgress', $agent->user_id)}}"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="{{route('discussionBoard', $agent->user_id)}}"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach    
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="text-md-right text-left mb-2">
                                                <a href="{{ route('inActiveAgents', 'type=myagents') }}" class="site-btn lg-btn blue">INACTIVE AGENTS</a>
                                                <a href="{{ route('getRegisterAgent', 'type=myagents') }}" class="site-btn lg-btn orange text-uppercase ml-2">register AGENTS</a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>AGENT ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FEE</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    <?php $j = 1; ?>
                                                    @if(count($data['myAgents']))
                                                    @foreach($data['myAgents'] as $myAgent)
                                                        <tr class="my_agent_tr">
                                                            <td>{{ $j++ }}</td>
                                                            <td class="my_agent_id">{{ $myAgent->user_id }}</td>
                                                            <td>{{ $myAgent->first_name.' '.$myAgent->last_name }}</td>
                                                            <td><?php echo $dtime =  $notificationService::dateTimeFormat($myAgent->created_at);?></td>                                                        
                                                            <td>{{ $myAgent->fee_enabled }}</td>
                                                            <td>{{ ($myAgent->manager_id)?$myAgent->manager_id:"" }}</td>
                                                            <td>{{($myAgent->manager_id)?$helper::getManagerName($myAgent->manager_id):""}}</td>                                                        
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item myagent_inactive" href="" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                        <a class="dropdown-item assign-task" data-toggle="modal" data-target=".assignTask"><i class="fas fa-edit"></i>ASSIGN NEW TASK</a>
                                                                        <a href="{{route('agentProfile', $myAgent->user_id)}}" class="dropdown-item" ><i class="fa fa-user-circle" aria-hidden="true"></i>View Profile</a>
                                                                        <a class="dropdown-item" href="{{route('viewAssignedActivities', $myAgent->user_id)}}"><i class="fas fa-clipboard-list"></i>View Assigned Activities</a> 
                                                                        <a class="dropdown-item" href="{{route('viewMonthlyProgress', $myAgent->user_id)}}"><i class="fas fa-chart-line"></i>View Monthly Progress</a>
                                                                        <a class="dropdown-item" href="{{route('discussionBoard', $myAgent->user_id)}}"><i class="fas fa-comment"></i>View Discussion Board</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach    
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this agent's profile ?</p>
                <div class="modal-btn-div">
                <form action="{{ url('admin/agent-inactive') }}" method="POST">
                @csrf
                <input type="hidden" class="agent" name="agent" value="">
                <input type="submit" class="site-btn orange" name="submit" value="Yes">
                                            
                </form>
                    <!-- <a class="site-btn orange" id="yesinactive">Yes</a> -->
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent xyz Profile has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
        <form action="{{route('assignAgentActivity')}}" method="POST">
                {{csrf_field()}}
                <input type = "hidden" name = "agent_id" class="agent-id-for-activity"> 
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border selectCat" name="" >
                    <option value="0">--SELECT CATEGORY--</option>
                    @if($data['cats'])
                        @foreach($data['cats'] as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    @endif    
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <i class="fas fa-search left-icon"></i>
                    <select class="site-input border both-icon activities-dropdown" name="activity_id" >
                    </select>
                    
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="goal" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                <button class="site-btn blue" type = "submit">ASSIGN</button>

                </div>
                </form>
        </div>
    </div>
</div>

@endsection