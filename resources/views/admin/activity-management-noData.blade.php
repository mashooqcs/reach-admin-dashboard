@extends('admin.layout.app')
@section('title', __('Activity management no data'))
@section('content')
<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">ACTIVITY MANAGEMENT</h1>
                                        </div>
                                    </div>
                                    <div class="text-md-right text-center">
                                        <a href="block-users.html" class="site-btn blue mb-1">ADD NEW</a>
                                    </div>
                                    <div class="empty-div">
                                        <h1>No Data Found</h1>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
