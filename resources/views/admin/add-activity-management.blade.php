<?php 
$title = 'Activity management';
$pg = 'activity';
?>
@extend
@extends('admin.layout.app')
@section('title', __('Add activity managegment'))
@section('content')
<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                        @if(Session::has('success'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                        @elseif(Session::has('error'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                                        @endif
                                        @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            <h1 class="mb-2"><a href="{{route('activity.management.index')}}"><i class="fas fa-chevron-left mr-1"></i> ACTIVITY MANAGEMENT</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>add new</p>
                                        </div>
                                      
                                        <div class="card-area">
                                        <form method = "POST" action = "{{route('addActivity')}}"> 
                                        {{csrf_field()}}
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">

                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Activity Name*</label>
                                                        <input type="text" class="site-input" name = "activity_name" placeholder="Enter Activity Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Activity Description*</label>
                                                        <input type="text" class="site-input" name = "activity_des" placeholder="Enter Description">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Category*</label>
                                                    <div class="form-field">
                                                        <div class="form-field mb-0">
                                                            <select required name="category_id" id="" class="site-input">
                                                            <option value="">Select</option>
                                                            @if(count($data))
                                                                @foreach($data as $cat)
                                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                                @endforeach
                                                            @endif
                                                            </select>
                                                            <i class="fas fa-caret-down right-icon"></i>
                                                        </div>                                    
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Status*</label>
                                                    <div class="form-field">
                                                        <div class="form-field mb-0">
                                                            <select name="status" id="" class="site-input">
                                                                <option value="">Select</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">Inactive</option>
                                                            </select>
                                                            <i class="fas fa-caret-down right-icon"></i>
                                                        </div>                                    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                            <button type = "submit" class="site-btn blue">Add</button>
                                            </div>

                                            </form>
                                        
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection