<?php 
    $pg = 'login';
    $title = "Login";
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> R.E.A.C.H | <?php echo ((isset($title))?$title:'Dashboard'); ?> </title>
    <link rel="shortcut icon" href="{{asset('assets/admin/images/favicon.png')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/intlTelInput.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/CustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
  <!-- END Custom CSS-->
</head>

<body class="<?php if ($title=="Login" || $title=="Register" || $title=="Error404" )  {echo "pt-0";} else {echo "";}?>" data-open="click" data-menu="vertical-menu" data-col="2-columns">


  <section class="login-bg">
    <div class="container">
        <div class="login-card">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 col-12 pl-0 pr-0 login-left-col">
                </div>
                <div class="col-md-6 col-12 pl-0 pr-0 login-right-col">
                    <div class="login-right-content">
                        <h2 class="login-card-heading doctor">Login</h2>
                        <form method="POST" action="{{ route('login') }}">
            @csrf
            <input type = "hidden" name="timezone" class="timezone" id="timezone">

                        <h5 class="login-subheading text-center black-text text-capitalize">Login in to your account</h5>
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block" id="alert_success_session">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block" id="alert_success_session">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <div class="form-field">
                            <input required = "required" type="email" class="site-input login left-icon" placeholder="Enter Email Address" name="email" id="">
                            <i class="fa fa-envelope left-icon" aria-hidden="true"></i>
                        </div>
                        <div class="form-field">
                            <i class="fa fa-lock left-icon" aria-hidden="true"></i>
                            <input required = "required" type="password" class="site-input login both-icon enter-input inputpassword" placeholder="Enter Password" name="password" id="">
                            <i class="fa fa-eye-slash enter-icon right-icon passeye" aria-hidden="true"></i>
                        </div>
                        <div class="below-input-div">
                            <p class="black-text mb-0">
                                <input type="checkbox" id="stopover" name="radio-group">
                                <label for="stopover" class="bordered mb-0">Remember Me</label>
                            </p>
                            <a href="#" class="forgot-link" data-toggle="modal" data-target="#pwdrecovery1">Forgot Password ?</a>
                        </div>
                       <button type = "submit" class="login-btn d-block text-center text-uppercase btn-block">Login</button>
                        <div class="col-12 text-center">
                            <a href="" class="back-link d-inline-block mt-4"><i class="fa fa-arrow-left mr-2" aria-hidden="true"></i>back to Homepage</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


        <!-- Password Recovery Modal 1 -->
        <div class="modal fade" id="pwdrecovery1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered site-modal" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="site-modalheading">Password Recovery</h2>
                    <div class="form-field">
                        <i class="fa fa-envelope left-icon" aria-hidden="true"></i>
                        <input type="email" required class="site-input login left-icon pwd-recovery-email" placeholder="Enter Email Address" name="" id="">
                        <span style = "display:none; color:red;" class = "email-validate-span">* Invalid Email</span>
                        <span style = "display:none; color:red;" class = "invalid-email-validate-span">* User does not exists with this Email Address.</span>


                    </div>
                    <a style = "display:none;" class="login-btn mt-2 pwd-recovery-email-continue" id="pwd1-btn">Continue</a>
                    <div class="text-center">
                        <a href="#" class="back-link d-inline-block mt-2" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Back to Login</a>
                    </div>
                </div>
            </div>
        </div>

      <!-- Password Recovery Modal 2 -->
      <div class="modal fade" id="pwdrecovery2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered site-modal" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h2 class="site-modalheading">Password Recovery</h2>
                  <div class="form-field mb-1">
                      <i class="fa fa-pen left-icon" aria-hidden="true"></i>
                      <input type="hidden" name = "pwd-recovery-email" class = "hidden-pwd-recovery-email">
                      <input type="number" class="site-input login left-icon verification-code" placeholder="Enter Verification Code" name="" id="">
                  </div>
                  <span style = "display:none; color:red;" class="code-validate-span">* Invalid Code</span>
                  <span style = "display:none; color:green;" class="code-valid-span">* Code matched successfully</span>

                  <a href="#" class="forgot-link d-block text-right mb-1">Didn't Receive Code? Send it Again</a>
                  <a style = "display:none;" href="#" class="login-btn mt-2 verify-code-continue" id="pwd2-btn">Continue</a>
                  <div class="text-center">
                      <a href="#" class="back-link d-inline-block mt-3" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Back to Login</a>
                  </div>
              </div>
          </div>
      </div>

        <!-- Password Recovery Modal 3 -->
        <div class="modal fade" id="pwdrecovery3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered site-modal" role="document">
              <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h2 class="site-modalheading">Password Recovery</h2>
                 <form action = "{{route('passwordRecover')}}" method = "POST">
                 {{csrf_field()}} 
                  <div class="form-field">
                    <input type = "hidden" name = "recovery-email" value = "" class = "final-hidden-pwd-recovery-email">
                      <i class="fa fa-lock left-icon" aria-hidden="true"></i>
                      <input required type="password" class="inputpassword1 site-input login both-icon confirm-input" placeholder="Enter Password" name="password" >
                      <i class="passeye1 fa fa-eye-slash confirm-icon right-icon" aria-hidden="true"></i>
                  </div>
                  <div class="form-field">
                      <i class="fa fa-lock left-icon" aria-hidden="true"></i>
                      <input required type="password" class="inputpassword2 site-input login both-icon enter-input" placeholder="Retype Password" name="confirm_password" >
                     <i class="passeye2 fa fa-eye-slash right-icon" aria-hidden="true"></i>
                  </div>
                  <button class="login-btn btn-block" type = "submit" >update</button>
                  <div class="text-center">
                      <a href="#" class="back-link d-inline-block mt-3" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-left mr-1" aria-hidden="true"></i>Back to Login</a>
                  </div>
                 </form> 
              </div>
            </div>
        </div>

<!-- BEGIN VENDOR JS--> 
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/admin/app-assets/vendors/js/vendors.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/core/app-menu.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/core/app.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/customizer.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/jquery.repeater.min.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/form-repeater.js')}}" ></script> 
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="{{asset('assets/admin/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<!-- <script src="{{asset('assets/admin/app-assets/vendors/js/extensions/moment.min.js')}}" ></script>  -->


<script src="{{asset('assets/admin/app-assets/js/scripts/modal/components-modal.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/intlTelInput.js')}}"></script> 
<script src="{{asset('assets/admin/assets/js/function.js')}}" ></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone-with-data-2012-2022.min.js"></script>

<script>

    $(document).ready(function(){

        $('.passeye').click(function(){
          
            let elem  = $(".inputpassword");
            if ('password' == $(elem).attr('type')) {
                $(elem).prop('type', 'text');
                $(this).removeClass("fa-eye-slash");
                $(this).addClass("fa-eye");
            } else {
                $(elem).prop('type', 'password');
                $(this).removeClass("fa-eye");
                $(this).addClass("fa-eye-slash");      
            }
        }); 


         $('.passeye1').click(function(){
            let elem1 = '';
            elem1  = $(".inputpassword1");
            if ('password' == $(elem1).attr('type')) {
                $(elem1).prop('type', 'text');
                $(this).removeClass("fa-eye-slash");
                $(this).addClass("fa-eye");
            } else {
                $(elem1).prop('type', 'password'); 
                $(this).removeClass("fa-eye");
                $(this).addClass("fa-eye-slash");     
            }
        });   
        $('.passeye2').click(function(){
             elem2  = $(".inputpassword2");
            if ('password' == $(elem2).attr('type')) {
                $(elem2).prop('type', 'text');
                $(this).removeClass("fa-eye-slash");
                $(this).addClass("fa-eye");
            } else {
                $(elem2).prop('type', 'password'); 
                $(this).removeClass("fa-eye");
                $(this).addClass("fa-eye-slash");   
            }
        });
        

        $('.pwd-recovery-email').on('keyup', function(){
           email =  $(".pwd-recovery-email").val();

            if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.][a-zA-Z0-9-]/.test(email))
            {
                
                $('.email-validate-span').css("display", "none");

                $.ajax({
                    url:"check-email/"+email,
                    method: 'GET',
                    data:{},
                    success: function(data){
                        if(data.msg=='email_found'){
                            // $('#pwd2-btn').css({'display':'block'});
                            $('.code-validate-span').css("display", "none");
                            $('.pwd-recovery-email-continue').css("display", "block");
                            $('.invalid-email-validate-span').css("display", "none");

                        }
                        else{
                            
                            $('.invalid-email-validate-span').css("display", "block");
                            $('.verify-code-continue').css("display", "none");
                            $('.pwd-recovery-email-continue').css("display", "none");

                        }

                    }
                });

            }
            else{
                $('.invalid-email-validate-span').css("display", "none");
                $('.email-validate-span').css("display", "block");
                $('.pwd-recovery-email-continue').css("display", "none");
            }

            
        });

    });


    $(".verification-code").bind("paste", function(e){
    // access the clipboard using the api
    let code = e.originalEvent.clipboardData.getData('text');

    let email   =   $('.hidden-pwd-recovery-email').val();
        // let code    =   $('.verification-code').val(); 


    $.ajax({
            url:"check-password-recovery-code/"+email+"/"+code,
            method: 'GET',
            data:{},
            success: function(data){
                if(data.msg=='code matched'){
                    $('.verify-code-continue').css({'display':'block'});
                    $('.code-validate-span').css("display", "none");
                    $('.code-valid-span').css("display", "block");

                }
                else{
                    $('.code-validate-span').css("display", "block");
                    $('.verify-code-continue').css("display", "none");
                    $('.code-valid-span').css("display", "none");

                }

            }
        });

        $('.verify-code-continue').on('click', function() {
            let email = $('.hidden-pwd-recovery-email').val();
            $('.final-hidden-pwd-recovery-email').val(email);
        });
} );


    $('.pwd-recovery-email-continue').click(function(){
    
    let  email =  $(".pwd-recovery-email").val();
    $(".hidden-pwd-recovery-email").val(email);

    $.ajax({
            url:"send-password-recovery-code/"+email,
            method: 'GET',
            data:{},
            success: function(data){
            }
        });
 
   });

   $('.verification-code').on('keyup', function() {
        let email   =   $('.hidden-pwd-recovery-email').val();
        let code    =   $('.verification-code').val(); 


    $.ajax({
            url:"check-password-recovery-code/"+email+"/"+code,
            method: 'GET',
            data:{},
            success: function(data){
                if(data.msg=='code matched'){
                    $('.verify-code-continue').css({'display':'block'});
                    $('.code-validate-span').css("display", "none");
                    $('.code-valid-span').css("display", "block");

                }
                else{
                    $('.code-validate-span').css("display", "block");
                    $('.verify-code-continue').css("display", "none");
                    $('.code-valid-span').css("display", "none");

                }

            }
        });

        $('.verify-code-continue').on('click', function() {
            let email = $('.hidden-pwd-recovery-email').val();
            $('.final-hidden-pwd-recovery-email').val(email);
            // $(this).hide();
            // $('.verification-code').val('');
        });

        // $('.verify-code-continue').on('click', function() {
        //     // $(this).hide();
        //     // $('.verification-code').val('');

        // });

   });
  
 
    </script>
    <script>
       $(document).ready(function(){
            $('#timezone').val(moment.tz.guess());
        });       
    
    </script>
</body>
</html>