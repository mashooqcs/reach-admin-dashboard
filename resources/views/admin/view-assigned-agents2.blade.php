@extends('admin.layout.app')
@section('title', __('View assigned agents 2'))
@section('content')


<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="inactive-managers.php"><i class="fas fa-chevron-left mr-1"></i>INACTIVE MANAGERS</a></h1>
                                        </div>
                                    </div>
                                    <div class="profile-picture-div pb-4">
                                        <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                        <p class="form-heading pt-1 mb-0">001</p>
                                        <p class="form-heading">John Smith</p>
                                    </div>
                                    <p class="form-heading pl-1 mb-2">Agent Log</p>
                                    <div class="row ml-0 mr-0 align-items-end">
                                                <div class="col-12">
                                                    <label  for="">Sort By:</label>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <label  for="">From:</label>
                                                    <input id="datepicker-1" class="site-input border" type="text" readonly>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <label  for="">To:</label>
                                                    <input id="datepicker-2" class="site-input border" type="text" readonly>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <a href="block-users.html" class="site-btn orange mb-1">APPLY/CLEAR</a>
                                                </div>
                                                <!-- <div class="col-xl-8 col-lg-6 col-12 text-md-right">
                                                    <div class="btn-with-filter">
                                                        <a href="block-users.html" class="site-btn green">BLOCK USERS</a>
                                                        <select name="" id="" class="filter-select mb-0">
                                                            <option value="">Filter by Status</option>
                                                            <option value="">Select 1</option>
                                                            <option value="">Select 2</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>AGENT ID</th>
                                                            <th>AGENT NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>Nov 20 ,2020</td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>Nov 20 ,2020</td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>001</td>
                                                            <td>Steve James</td>
                                                            <td>Nov 20 ,2020</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
