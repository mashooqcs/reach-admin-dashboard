<?php 
$title = 'Recieved request details';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Recieved request details'))
@section('content')

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('managersRequests')}}"><i class="fas fa-chevron-left mr-1"></i> RECIEVED PROFILE</a></h1>
                                        </div>
                                    </div>
                                    <div class="profile-picture-div">
                                    @if($data->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif                                             </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>Verification Request Id: {{$data->id}}</p>
                                        </div>
                                      
                                        <div class="card-area">
                                        @if ($message = Session::get('success'))
                                        <div class="alert alert-success alert-block" id="alert_success_session">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @endif

                                            <p class="form-heading orange-text pt-2">Personal Information</p>
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">First Name</label>
                                                            <p class="site-text">{{$data->first_name}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Last Name</label>
                                                            <p class="site-text">{{$data->last_name}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Phone Number</label>
                                                            <p class="site-text">{{$data->phone_number}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Email Address</label>
                                                            <p class="site-text">{{$data->email}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="form-heading orange-text pt-2">Address Details</p>
                                                <div class="row">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Address</label>
                                                            <p class="site-text">{{$data->address}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Country</label>
                                                            <p class="site-text">{{$data->country}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">State</label>
                                                            <p class="site-text">{{$data->state}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">City</label>
                                                            <p class="site-text">{{$data->city}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-field">
                                                            <label for="" class="site-label">Zip Code</label>
                                                            <p class="site-text">{{$data->zip_code}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                    
                                                <p class="black-text mb-0">
                                                    <input type="checkbox" id="stopover" name="radio-group">
                                                    <label for="stopover" class="bordered mb-0">Enable registration fee</label>
                                                </p>
                                                <div class="text-center mt-3">
                                                    <a data-toggle="modal" data-target=".registerManager" class="site-btn blue mr-sm-2 mb-sm-0 mb-2">Register</a>
                                                    <a data-toggle="modal" data-target=".rejectionReason" class="site-btn orange register-rejection">Register Rejection</a>
                                                </div>
                                            </div>           
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Manager Modal -->
<div class="modal fade registerManager" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this Manager?</p>
                <div class="modal-btn-div">
                <form action ="{{route('rejectManagersRequest')}}"  method = "POST">
                {{csrf_field()}}
            <input type  = "hidden" name = "manager_id" value = "{{$data->id}}"> 
            <input type  = "hidden" name = "type" value = "accept"> 
                    <button type = "submit" class="site-btn orange yesmanager" >Yes</button>
                </form>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Manager Confirmation -->
<div class="modal fade yesregisterManager1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Manager has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Reason -->
<div class="modal fade rejectionReason" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
            </div>
            <form action ="{{route('rejectManagersRequest')}}" method = "POST">
            {{csrf_field()}}
            <input type  = "hidden" name = "manager_id" value = "{{$data->id}}"> 
            <input type  = "hidden" name = "type" value = "reject"> 
 
                <div class="form-field">
                    <label for="" class="site-label font-weight-bold">Enter Rejection Reason</label>
                    <textarea name="reason" id="" cols="30" rows="10" class="site-input border" placeholder="Enter Rejection Reason"></textarea>
                </div>
                <div class="modal-btn-div">
                <button class="site-btn orange" type = "submit">SUBMIT</button>
                    </form>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">CANCEL</a>
                </div>

        </div>
    </div>
</div>

@endsection