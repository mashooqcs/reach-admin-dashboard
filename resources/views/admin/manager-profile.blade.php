<?php 
$title = 'Manager Profile';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Manager profile'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>



<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('managers.index')}}"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>view profile</p>
                                            <p>Manager id: {{$data->id}}</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <div class="text-info-area mb-2">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p><?php echo $notificationService::dateTimeFormat($data->created_at);?></p>
                                                    <a href="{{route('managerEditProfile', $data->id)}}" class="site-btn sm-btn blue">EDIT</a>
                                            </div>
                                            <div class="profile-picture-div">
                                            @if($data->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data->profile_image}}" class="profile-pic img-fluid" alt="">
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}" class="profile-pic img-fluid" alt="">
                                                    @endif 
                                                  
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name</label>
                                                        <input type="text" class="site-input" value="{{$data->first_name}}"readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name</label>
                                                        <input type="text" class="site-input" value="{{$data->last_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number</label>
                                                        <input type="number" id="" class="site-input" value="{{($data->phone_number)?$data->phone_number:''}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email</label>
                                                        <input type="email" class="site-input" value="{{$data->email}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address</label>
                                                        <input type="text" class="site-input" value="{{($data->address)?$data->address:''}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country</label>
                                                        <input type="text" class="site-input" value="{{($data->country)?$data->country:''}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State</label>
                                                        <input type="text" class="site-input" value="{{($data->state)?$data->state:''}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City</label>
                                                        <input type="text" class="site-input" value="{{($data->city)?$data->city:''}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" value="{{($data->zip_code)?$data->zip_code:''}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection