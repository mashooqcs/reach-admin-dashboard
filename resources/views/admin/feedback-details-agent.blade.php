<?php 
$title = 'Feedback';
$pg = 'feedback';
$authUser = Auth()->user();
?>

@extends('admin.layout.app')
@section('title', __('Feedback details agent'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="{{route('feedback.index')}}"><i class="fas fa-chevron-left mr-1"></i> FEEDBACK DETAILS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>feeback</p>
                                            <p>agent id: {{$data[0]->user_id}}</p>
                                        </div>
                                      
                                        <div class="card-area">
                                            <div class="text-md-right text-left">
                                                    <p class="form-heading mb-0">Registration Date :</p>
                                                    <p><?php echo $dtime =  $notificationService::dateTimeFormat($data[0]->created_at);?></p>

                                            </div>
                                            <div class="profile-picture-div">
                                                @if($data[0]->profile_image)
                                                        <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$data[0]->profile_image}}" class="profile-pic img-fluid" alt="avatar"> 
                                                    @else
                                                        <img src="{{asset('assets/admin/images/img-placeholder.png')}}"  class="profile-pic img-fluid" alt="avatar"> 
                                                    @endif
                                        
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" value="{{$data[0]->first_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input"  value="{{$data[0]->last_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" value="{{$data[0]->email}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Subject*</label>
                                                        <input type="text" class="site-input" value="{{$data[0]->subject}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" value="{{$data[0]->address}} {{$data[0]->city}} {{$data[0]->state}} {{$data[0]->country}}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection