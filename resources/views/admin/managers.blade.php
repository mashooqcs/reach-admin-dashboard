<?php 
$title = 'Managers';
$pg = 'managers';
?>
@extends('admin.layout.app')
@section('title', __('Managers'))
@section('content')
<?php $notificationService = app('App\Services\NotificationService'); ?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">Managers</h1>
                                        </div>
                                    </div>
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    <div class="three-btn-div">
                                        <a href="{{route('managersRequests')}}" class="site-btn lg-btn black">REQUESTS</a>
                                        <a href="{{route('inActiveManagers')}}" class="site-btn lg-btn blue">INACTIVE MANAGERS</a>
                                        <a href="{{route('createManager')}}" class="site-btn lg-btn orange text-uppercase">REGISTER MANAGER</a>
                                    </div>
                                    <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>REGISTRATION DATE</th>
                                                            <th>FEE</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data))
                                                    <?php  $i=1;?>
                                                        @foreach($data as $manager)
                                                            <tr class ="manager-tr">
                                                                <td>{{$i}}</td>
                                                                <td class="manager-id">{{$manager->user_id}}</td>
                                                                <td>{{$manager->first_name}}{{' '}}{{$manager->last_name}}</td>
                                                                <td><?php echo $notificationService::dateTimeFormat($manager->created_at);?></td>
                                                                <td></td>
                                                                <td>
                                                                    <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                        <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                        <div class="dropdown-menu custom-dropdown"> 
                                                                            <a class="dropdown-item deactivate-manager-status" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye-slash"></i>INACTIVE </a> 
                                                                            <a href="{{route('managerProfile', $manager->user_id)}}" class="dropdown-item" ><i class="fas fa-edit"></i>View Profile</a>
                                                                            <a class="dropdown-item" href="{{route('viewAssignedAgents', $manager->user_id)}}"><i class="fa fa-user-circle" aria-hidden="true"></i>View Assigned Agents</a> 
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php $i++; ?>
                                                        @endforeach
                                                    @endif        
                                                    </tbody>
                                                </table>
                                            </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this manager's profile ?</p>
                <input type = "hidden" name ='manager-id-input' class = "manager-id-input">
                <div class="modal-btn-div">
                    <a href="#" class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div tick">
                    <img src="./images/tick.png" class="modal-tick img-fluid" alt="">
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Manager Profile has been inactivated</p>
                <div class="modal-btn-div">
                <form method = "POST" action  ="{{route('updateUserStatus')}}">
                {{csrf_field()}}
                <input type = "hidden" name ='manager-id-input' class = "manager-id-input">
                <input type = "hidden" name ='type' value = "inactive">
                <button class="site-btn blue" type = "submit">GOT IT</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>

@endsection