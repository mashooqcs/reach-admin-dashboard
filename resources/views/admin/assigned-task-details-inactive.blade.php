@extends('admin.layout.app')
@section('title', __('Assinged task detail inactive'))
@section('content')

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="inactive-agents.php"><i class="fas fa-chevron-left mr-1"></i> INACTIVE AGENTS</a></h1>
                                        </div>
                                    </div>
                                    <div class="profile-picture-div pb-2">
                                        <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                        <p class="form-heading pt-1 mb-0">001</p>
                                        <p class="form-heading">John Smith</p>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>activity details</p>
                                            <p>activity id:001</p>
                                        </div>
                                        <div class="card-area">
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Activity Name </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">Abc </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Activity Category</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">Hourly </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Description</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard 
                                                            dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                                            It has survived not only five centuries. 
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Assigned Date</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">Aug-12-2020</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-4 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text">Start Date</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-10 col-sm-8 col-12">
                                                    <div class="form-field">
                                                        <p class="site-text black-text">June-12-2020</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress-info">
                                                <p class="site-text green-text">Actual : 10 Hours </p>
                                                <p class="site-text green-text">Goal : 20 Hours </p>
                                                <p class="site-text green-text">Complete : 50% </p>
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="card-heading">
                                        <p>Progress Breakdown</p>
                                    </div>
                                    <div class="card-area">
                                        <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>S.NO</th>
                                                            <th>DATE</th>
                                                            <th>DAY</th>
                                                            <th>DATA ADDED</th>
                                                            <th>Actual</th>
                                                            <th>Goal</th>
                                                            <th>Complete</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>May 2, 2020</td>
                                                            <td>Monday</td>
                                                            <td>2 Hours</td>
                                                            <td>2 Hours</td>
                                                            <td>20 Hours</td>
                                                            <td>10%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>May 2, 2020</td>
                                                            <td>Monday</td>
                                                            <td>2 Hours</td>
                                                            <td>2 Hours</td>
                                                            <td>20 Hours</td>
                                                            <td>10%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>May 2, 2020</td>
                                                            <td>Monday</td>
                                                            <td>2 Hours</td>
                                                            <td>2 Hours</td>
                                                            <td>20 Hours</td>
                                                            <td>10%</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection