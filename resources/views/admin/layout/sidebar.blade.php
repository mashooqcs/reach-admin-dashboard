<?php if(!isset($pg)){$pg ='dashboard';}; ?>
<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a
                        class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                            class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item"> <a class="navbar-brand" href="{{url('admin/dashboard')}}"> <img class="brand-logo img-fluid"
                            alt="stack admin logo" src="{{asset('assets/admin/images/logo.png')}}"> </a> </li>
                <li class="nav-item d-md-none"> <a class="nav-link open-navbar-container" data-toggle="collapse"
                        data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a> </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-h" id="temppp">
                        <a class="nav-link nav-link-label" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown">
                            <i class="far fa-bell"></i>
                        </a>
                        <div class="dropdown-menu reminder-dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <p class="form-heading text-center">Reminder</p>
                          
                                <ul class="nav nav-pills reminder-pills" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-active-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-active" aria-selected="true">Active</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-new-tab" data-toggle="pill" href="#pills-new" role="tab" aria-controls="pills-new" aria-selected="false">New</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-inactive-tab" data-toggle="pill" href="#pills-inactive" role="tab" aria-controls="pills-inactive" aria-selected="false">Inactive</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-active" role="tabpanel" aria-labelledby="pills-active-tab">
                                        <div class="text-center my-2">
                                            <a href="#" class="reminder-btn">Reminder</a>
                                        </div>
                                        <div class="reminder-scroller">

                                        <?php $getReminders = $notificationService::getReminders(); ?>

                                        @if(count($getReminders['active']))
                                        @foreach($getReminders['active'] as $reminder)

                
                                            <div class="reminder-box">
                                                <div class="reminder-row">
                                                    <p>{{$reminder->title}}</p>
                                                    <div class="reminder-right">
                                                        <p><?php echo $notificationService::dateTimeFormat($reminder->created_at); ?></p>
                                                    </div>
                                                </div>
                                                <div class="reminder-row">
                                                    <p>{{$reminder->repeat_on}}</p>
                                                    <div class="reminder-right">
                                                        <a href="#">
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target=".deleteReminder">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif    
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-new" role="tabpanel" aria-labelledby="pills-new-tab">
                                       
                                        <div class="text-center my-2">
                                            <a href="#" class="reminder-btn">Add New Reminder</a>
                                        </div>

                                        <form method = "POST" action= "{{route('addReminder')}}">
                                        {{csrf_field()}}

                                        
                                        <div class="form-field">
                                            <label for="" class="site-label">Title*</label>
                                            <input required type="text" class="site-input" placeholder="Enter Title" name = "title">
                                        </div>
                                        <div class="form-field">
                                            <label for="" class="site-label">Date*</label>
                                            <input required type="text" class="site-input" id="datepicker-4" placeholder="Select Date" name = "start_date">
                                        </div>
                                        <div class="form-field">
                                            <label for="" class="site-label">Select Time*</label>
                                            <input required type="text" class="site-input" id="timepicker" placeholder="Select Time" name = "start_time">
                                        </div>
                                        <label for="" class="site-label">Repeat On*</label>
                                        <div class="form-field">
                                            <select required name="repeat_on" id="" class="site-input">
                                                <option value="Everyday">Everyday</option>
                                                <option value="Saturday">Saturday</option>
                                                <option value="Sunday">Sunday</option>
                                                <option value="Monday">Monday</option>
                                                <option value="Tuestday">Tuestday</option>
                                                <option value="Wednesday">Wednesday</option>
                                                <option value="Thursday">Thursday</option>
                                                <option value="Friday">Friday</option>
                                            </select>
                                            <i class="fas fa-caret-down right-icon"></i>
                                        </div>
                                        <div class="text-center">
                                        <button type = "submit" class="site-btn blue add-reminder"  >ADD</button>
                            
                                        </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="pills-inactive" role="tabpanel" aria-labelledby="pills-inactive-tab">
                                        <div class="text-center my-2">
                                            <a href="#" class="reminder-btn">Reminder</a>
                                        </div>
                                        @if(count($getReminders['inactive']))
                                            @foreach($getReminders['inactive'] as $reminder)
                                                <div class="reminder-scroller">
                                                    <div class="reminder-box">
                                                        <div class="reminder-row">
                                                            <p>{{$reminder->title}}</p>
                                                            <div class="reminder-right">
                                                            <p><?php echo $notificationService::dateTimeFormat($reminder->created_at); ?></p>

                                                            </div>
                                                        </div>
                                                        <div class="reminder-row">
                                                            <p>{{$reminder->repeat_on}}</p>
                                                            <div class="reminder-right">
                                                                <a href="#">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target=".deleteReminder">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="dropdown dropdown-notification nav-item two-bell-icons">
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                          <i class="fa fa-bell" aria-hidden="true"></i> 
                          <!-- <span class="badge badge-pill badge-default badge-danger badge-default badge-up">5</span>  -->
                        </a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0">
                                    <span class="grey darken-2">Notifications</span>
                                    <!-- <span class="notification-tag badge badge-default badge-danger float-right m-0">5 New</span>  -->
                                </h6>
                            </li>
                            <li class="scrollable-container media-list ps-container ps-theme-dark ps-active-y"
                                data-ps-id="75e644f2-605d-3ecc-f100-72d86e4a64d8">
                                
                                <?php $notifications = $notificationService::getRecentNotifications(); ?>

                                @if(count($notifications))
                                @foreach($notifications as $notification)
                                <a href="{{route('myNotificaions')}}">
                                    <div class="media">
                                        <div class="media-left align-self-center">
                                            <i class="fas fa-bell notifications-bell"></i>
                                        </div>
                                        <div class="media-body">
                                            <!-- <h6 class="media-heading">You have new notification!</h6> -->
                                            <p class="notification-text font-small-3 text-muted">{{$notification->description}}</p>
                                            <div class="notification-below-info">
                                                <small>
                                                    <time class="media-meta text-muted"
                                                        datetime="2015-06-11T18:29:20+08:00"><?php echo $notificationService::dateTimeFormat($notification->created_at); ?></time>
                                                </small>
                                                
                                            </div>
                                        </div>
                                        <div class="media-right align-self-center">
                                            <i class="fas fa-eye"></i>
                                        </div>

                                    </div>
                                </a>
                                @endforeach()
                               @endif

                            </li>
                            <li class="dropdown-menu-footer"><a
                                    class="dropdown-item notification text-muted text-center"
                                    href="{{route('myNotificaions')}}">View All</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item"> 
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> 
                            <span class="avatar avatar-online"> 
                            @if($authUser->profile_image)
                                <img src="{{asset('assets/admin/images/')}}{{'/'}}{{$authUser->profile_image}}" alt="avatar"> 
                            @else
                                <img src="{{asset('assets/admin/app-assets/images/portrait/small/avatar-s-1.png')}}" alt="avatar"> 
                            @endif
                            </span> 
                            <span class="user-name">{{$authUser->first_name}}{{' '}}{{$authUser->last_name}}</span> 
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{url('admin/my-profile')}}"><i class="fas fa-user-circle"></i>Profile</a>
                            <a class="dropdown-item" data-toggle="modal" data-target=".logoutModal"><i class="fas fa-sign-out-alt"></i>Logout</a>
                        </div>
                    </li>
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                            <i class="ft-menu"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item <?php if ($pg=="dashboard") {echo "active"; } else  {echo "";}?>"><a href="{{url('admin/dashboard')}}"><i class="fas fa-chart-area"></i><span class="menu-title" data-i18n="">Dashboard</span></a></li>
            <li class="nav-item <?php if ($pg=="agents") {echo "active"; } else  {echo "";}?>"><a href="{{url('admin/agents')}}"><i
                        class="fas fa-user-circle"></i><span class="menu-title" data-i18n="">Agents</span></a></li>
            <li class="nav-item <?php if ($pg=="managers") {echo "active"; } else  {echo "";}?>"><a
                    href="{{url('admin/managers')}}"><i class="fas fa-user"></i><span class="menu-title"
                        data-i18n="">managers</span></a></li>
            <li class="nav-item <?php if ($pg=="activity") {echo "active"; } else  {echo "";}?>"><a
                    href="{{url('admin/activity-management')}}"><i class="fas fa-calendar-check"></i><span class="menu-title"
                        data-i18n="">activity management</span></a></li>
            <li class="nav-item <?php if ($pg=="category") {echo "active"; } else  {echo "";}?>"><a
                    href="{{url('admin/category-management')}}"><i class="fas fa-clipboard-list"></i><span class="menu-title"
                        data-i18n="">category management</span></a></li>
            <li class="nav-item <?php if ($pg=="payment") {echo "active"; } else  {echo "";}?>"><a
                    href="{{url('admin/payment-log')}}"><i class="fas fa-credit-card"></i><span class="menu-title"
                        data-i18n="">payment log</span></a></li>
            <li class="nav-item <?php if ($pg=="settings") {echo "active"; } else  {echo "";}?>">
                <a href="{{url('admin/settings')}}">
                    <i class="fas fa-cog"></i><span class="menu-title" data-i18n="">settings</span>
                </a>
            </li>
            <li class="nav-item <?php if ($pg=="feedback") {echo "active"; } else  {echo "";}?>"><a
                    href="{{url('admin/feedback')}}"><i class="fas fa-address-card"></i><span class="menu-title"
                        data-i18n="">feedback</span></a></li>

        </ul>
    </div>
</div>

<!-- Delete Reminder Modal -->
<div class="modal fade deleteReminder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to delete this Reminder ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesDeleteReminder">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Reminder Deleted Modal -->
<div class="modal fade confirmDeleteReminder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Reminder has been deleted</p>
                <div class="modal-btn-div">
                    <!-- <a class="site-btn orange" id="yesDeleteReminder">Yes</a> -->
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Logout Modal -->
<div class="modal fade logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to Logout ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" href="{{route('logout')}}">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Reminder Added Modal -->
<div class="modal fade reminderAdded" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Reminder has been added</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>