<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> R.E.A.C.H | <?php echo ((isset($title))?$title:'Dashboard'); ?> </title>
    <link rel="shortcut icon" href="{{asset('assets/admin/images/favicon.png')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/intlTelInput.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/assets/css/CustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
  <!-- END Custom CSS-->
</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar <?php //if ($title=="Login" || $title=="Register" || $title=="Error404" )  {echo "pt-0";} else {echo "";}?>" data-open="click" data-menu="vertical-menu" data-col="2-columns">

<?php  $authUser = Auth()->user(); ?>
<?php $notificationService = app('App\Services\NotificationService'); ?>

@include('admin.layout.sidebar')
@yield('content')


<!-- ///////////////////////////////////////////////////////////////s/////////////--> 

<!-- BEGIN VENDOR JS--> 
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{asset('assets/admin/app-assets/vendors/js/vendors.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/vendors/js/charts/chart.min.js')}}"></script> 
<script src="{{asset('assets/admin/app-assets/js/core/app-menu.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/core/app.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/customizer.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/chart.js')}}"></script> 
<script src="{{asset('assets/admin/assets/js/jquery.repeater.min.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/form-repeater.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/chart.js')}}" ></script> 
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="{{asset('assets/admin/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{asset('assets/admin/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/vendors/js/charts/echarts/echarts.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/vendors/js/extensions/moment.min.js')}}" ></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/charts/chartjs/line/line.js')}}"></script> 
<script src="{{asset('assets/admin/app-assets/js/scripts/charts/chartjs/line/line-area.js')}}"></script> 
<!-- <script src="app-assets/js/scripts/charts/chartjs/line/line-logarithmic.js"></script> -->
<!-- <script src="app-assets/js/scripts/charts/chartjs/line/line-multi-axis.js"></script> -->

<script src="{{asset('assets/admin/app-assets/js/scripts/modal/components-modal.js')}}" ></script> 
<script src="{{asset('assets/admin/assets/js/intlTelInput.js')}}"></script> 
<script src="{{asset('assets/admin/assets/js/function.js')}}" ></script> 

<!-- BEGIN VENDOR JS--> 
<!-- BEGIN VENDOR JS--> 
<!-- BEGIN PAGE VENDOR JS--> 
<!-- END PAGE VENDOR JS--> 
<!-- BEGIN STACK JS--> 
<script>
        // Set paths
        // ------------------------------

        require.config({
            paths: {
                echarts: 'admin/app-assets/vendors/js/charts/echarts'
            }
        });


        // Configuration
        // ------------------------------

        require(
            [
                'echarts',
                'echarts/chart/bar',
                'echarts/chart/line',
                'echarts/chart/scatter',
                'echarts/chart/pie'
            ],

            // Charts setup
            function(ec) {

                // Initialize chart
                // ------------------------------
                var myChart = ec.init(document.getElementById('column-chart'));

                // Chart Options
                // ------------------------------
                chartOptions = {

                    // Setup grid
                    grid: {
                        x: 60,
                        x2: 60,
                        y: 45,
                        y2: 25
                    },

                    // Add tooltip
                    tooltip: {
                        trigger: 'axis'
                    },

                    // Add legend

                    // Add custom colors
                    color: ['#ff6b00', '#2e5eb6', '#11b04f'],

                    // Enable drag recalculate
                    calculable: true,

                    // Horizontal axis
                    xAxis: [{
                        type: 'category',
                        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    }],

                    // Vertical axis
                    yAxis: [{
                            type: 'value',
                            // name: 'Water',
                            axisLabel: {
                                formatter: '{value} K'
                            }
                        },
                        {
                            type: 'value',
                            name: 'Temperature',
                            axisLabel: {
                                formatter: '{value} °C'
                            }
                        }
                    ],

                    // Add series
                    series: [{
                            name: 'Evaporation',
                            type: 'bar',
                            data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
                        },
                        {
                            name: 'Precipitation',
                            type: 'bar',
                            data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
                        },
                        
                    ]
                };

                // Apply options
                // ------------------------------

                myChart.setOption(chartOptions);


                // Resize chart
                // ------------------------------

                $(function() {

                    // Resize chart on menu width change and window resize
                    $(window).on('resize', resize);
                    $(".menu-toggle").on('click', resize);

                    // Resize function
                    function resize() {
                        setTimeout(function() {

                            // Resize chart
                            myChart.resize();
                        }, 200);
                    }
                });
            }
        );

    </script>

<script>
$(document).ready(function(){


     //Attaching event handler to .dropdown selector.
  $('.dropdown-h').on({
    //fires after dropdown is shown instance method is called (if you click anywhere else)
    'shown.bs.dropdown': function (e) {
        this.close = true;
       
    },

    //when dropdown is clicked
    click: function (e) {
        this.close = false;
    },

    //when close event is triggered
    'hide.bs.dropdown': function () {
       
      return this.close;
    }
  });

  window.onclick = function(event) {
    if(!document.getElementsByClassName('dropdown-h')[0].contains(event.target)||!document.getElementsByClassName('timepicker')){
        $(".dropdown-h").removeClass("show")
        $(".reminder-dropdown-menu").removeClass("show")
    }
  
  }
  $(".catEdit").click(function(){
    var catName  =  $(this).closest(".cat-tr").children(".cat-name").text();
    var catId = $(this).closest(".cat-tr").children(".cat-id").text();
    $(".edit-cat-input").val(catName);
    $(".edit-cat-id-input").val(catId);
  })


  $(".activity-edit").click(function(){
    var aname   = $(this).closest(".activity-tr").children(".activity-name").text();
    var aDes   = $(this).closest(".activity-tr").children(".activity-description").text();
    var aCatName   = $(this).closest(".activity-tr").children(".activity-category-name").text();
    var aId  = $(this).closest(".activity-tr").children(".activity-id").text();


    $(".activity-name-input").val(aname);
    $(".activity-description-input").val(aDes);
    $(".activity-id-input").val(aId);
    $(".activity-category-input").val(aCatName);
  })
    $(".deactivate-manager-status").click(function(){
        var managerId   = $(this).closest(".manager-tr").children(".manager-id").text();
        $(".manager-id-input").val(managerId);
    })

    $(".inactive").click(function(){
        var agentid = $(this).closest(".agent_tr").children(".agent_id").text();
        $(".agent").val(agentid);
    })
    $(".myagent_inactive").click(function(){
        var agentid = $(this).closest(".my_agent_tr").children(".my_agent_id").text();
        $(".agent").val(agentid);
    })

    

    
    $(".assign-task").click(function(){
        var agentid = $(this).closest(".agent_tr").children(".agent_id").text();
        $(".agent-id-for-activity").val(agentid);
        aler(agentid);
        
    })

    $('.selectCat').on('change', function() {
        var catId = this.value;
        $.ajax({
            url:"get-activity-by-category/"+catId,
            method: 'GET',
            data:{},
            success: function(data){
            
                    $(".activities-dropdown").empty();
                    $(".activities-dropdown").append(data);

            }
        });
    });

  
    $('.passeye').click(function(){
        let elem  = $(".inputpassword")
        if ('password' == $(elem).attr('type')) {
            $(elem).prop('type', 'text');
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
        } else {
            $(elem).prop('type', 'password');
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");      
        }
    });

    $('.passeye1').click(function(){
        let elem1  = $(".inputpassword1")
        if ('password' == $(elem1).attr('type')) {
            $(elem1).prop('type', 'text');
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
        } else {
            $(elem1).prop('type', 'password');  
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");     
        }
    });
    $('.passeye2').click(function(){
        let elem2  = $(".inputpassword2")
        if ('password' == $(elem2).attr('type')) {
            $(elem2).prop('type', 'text');
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
        } else {
            $(elem2).prop('type', 'password');
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");       
        }
    });
    $('.passeye3').click(function(){
        let elem3  = $(".inputpassword3")
        if ('password' == $(elem3).attr('type')) {
            $(elem3).prop('type', 'text');
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
        } else {
            $(elem3).prop('type', 'password');
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");      
        }
    });

     
    function readURL(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('.profile-pic').attr('src', e.target.result);
            }
             
            reader.readAsDataURL(input.files[0]); // convert to base64 string
          }
        }
        
        

  $(function() {
     $("input:file").change(function (){
        //  console.log(this);
        readURL(this);

     });
  });


  $('.add-reminder').click(function(e){
    // e.preventDefault();
    // alert('a');

  });





  

});

$('input[type="radio"][name=radio-group]').click(function () {
	if ($(this).attr("value") == "ABC") {
		$(".select-manager").hide('slow');
	}
	if ($(this).attr("value") == "PQR") {
		$(".select-manager").show('slow');

	}
});





</script>
</body>
</html>