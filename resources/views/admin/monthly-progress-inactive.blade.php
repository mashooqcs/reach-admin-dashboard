@extends('admin.layout.app')
@section('title', __('Monthly progress inactive'))
@section('content')


<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="inactive-agents.php"><i class="fas fa-chevron-left mr-1"></i> INACTIVE AGENTS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>Monthly Progress Report</p>
                                        </div>
                                        <div class="card-area full">
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <p class="form-heading pt-1 mb-0">001</p>
                                                    <p class="form-heading">John Smith</p>

                                                    <!-- <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        <input type="submit">
                                                    </form> -->
                                            </div>
                                            <!-- <p class="form-heading pt-1 mb-3">Personal Information</p> -->
                                            <div class="row">
                                                <div class="col-md-4 col-12">
                                                    <label for="" class="site-label">Select</label>
                                                    <div class="form-field">
                                                        <select class="site-input border" name="" id="">
                                                            <option value="">Numeric</option>
                                                            <option value="">Alphabetic</option>
                                                            <option value="">Numeric</option>
                                                        </select>
                                                        <i class="fas fa-caret-down right-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <label for="" class="site-label">Select</label>
                                                    <div class="form-field">
                                                        <select class="site-input border" name="" id="">
                                                            <option value="">Hourly</option>
                                                            <option value="">Numeric</option>
                                                        </select>
                                                        <i class="fas fa-caret-down right-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body chartjs">
                                                <canvas id="line-chart" height="500"></canvas>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection