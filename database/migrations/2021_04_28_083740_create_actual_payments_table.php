<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActualPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
           
            $table->string('agent_fee');
            $table->string('manager_fee');
            $table->enum('status', [
                0,
                1,
                2,
            ])->default(1)->comment = '0=inactive, 1=active, 2=deleted';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_payments');
    }
}
