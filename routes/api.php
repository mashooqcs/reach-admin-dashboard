<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Manager\UserController;
use App\Http\Controllers\Api\Manager\NotificationController;
use App\Http\Controllers\Api\Manager\ActivityController;
use App\Http\Controllers\Api\Manager\CategoryController;
use App\Http\Controllers\Api\Manager\PaymentLogController;
use App\Http\Controllers\Api\Manager\UserDiscussionController;
use App\Http\Controllers\Api\Manager\ReminderNotificationController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('manager')->group(function () {
    Route::get('user-login-status', function () {
        return response()->json(['message' => 'You are not logged in, please login first'], 401);
    });
    Route::post('login', [UserController::class, 'login'])->name('manager.login');
    Route::post('forgotPassword', [UserController::class, 'resetPasswordRequest']);
    Route::post('verifyCode', [UserController::class, 'verifyCode']);
    Route::post('updatePassword', [UserController::class, 'updatePassword']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('contact', [UserController::class, 'contactUs'])->name('manager.contactUs');
        Route::get('getProfile', [UserController::class, 'getMyProfile']);
        Route::post('changePassword', [UserController::class, 'changePassword']);
        Route::get('notifications', [NotificationController::class, 'getMyNotifications']);
        Route::post('updateProfile', [UserController::class, 'updateProfile']);
        Route::post('payRegistration', [UserController::class, 'payRegistration']);
        Route::get('getAgents', [UserController::class, 'getAgents']);
        Route::delete('removeAgent/{id}', [UserController::class, 'removeAgent']);
        Route::post('assignActivity', [ActivityController::class, 'assignAgentActivity']);
        Route::get('getAllCategories', [CategoryController::class, 'index']);
        Route::get('getAllActivitiesByCategory/{categoryId}', [ActivityController::class, 'getAllActivitiesByCategory']);
        Route::get('agentDetails/{agentId}', [UserController::class, 'agentDetails']);
        Route::get('paymentLogs', [PaymentLogController::class, 'paymentLogs']);
        Route::get('getActivities', [ActivityController::class, 'getAgentActivities']);
        Route::post('sendMessage', [UserDiscussionController::class, 'addDiscussion']);
        Route::get('getChat', [UserDiscussionController::class, 'discussionBoard']);
        Route::post('addReminder', [ReminderNotificationController::class, 'addReminder']);
        Route::get('tes', function(){
            return array('aaa'); 
        });

        Route::post('logout', [UserController::class, 'logout']);
    });    
});