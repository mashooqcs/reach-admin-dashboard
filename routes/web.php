<?php
use App\Http\Controllers\Admin\Auth\AuthController;

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ActualPaymentController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\PaymentLogController;
use App\Http\Controllers\Admin\ReminderNotificationController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\ActivityController;
use App\Http\Controllers\Admin\UserActivityProgressBreakdownController;
use App\Http\Controllers\Admin\UserDiscussionController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\Auth\RegisteredUserController;
use App\Http\Controllers\Admin\UserActivityController;



use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('admin')->group(function () {
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store']);
    Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');
    Route::get('/register', [RegisteredUserController::class, 'create'])->name('register');
    Route::post('/register', [RegisteredUserController::class, 'store'])->name('registerAdmin')->middleware('guest');            
    Route::get('send-password-recovery-code/{email}', [UserController::class, 'sendPasswordRecoveryCode']);
    Route::get('check-password-recovery-code/{email}/{code}', [UserController::class, 'checkPasswordRecoveryCode']);
    Route::post('update-password-recover', [UserController::class, 'passwordRecover'])->name('passwordRecover');
    Route::get('check-email/{email}', [UserController::class, 'checkEmail'])->name('checkEmail');

});


Route::group(['middleware' => 'prevent-back-history'],function(){

    Route::group(['middleware' => 'auth'], function () {
        Route::prefix('admin')->group(function () {
        
            Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index')->name('dashboard');
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
            //agent
            Route::get('agents', [UserController::class, 'agents'])->name('agents.index');
            Route::get('inactive-agents', [UserController::class, 'inActiveAgents'])->name('inActiveAgents');
            Route::get('register-agents', [UserController::class, 'viewRegisterAgent'])->name('getRegisterAgent');
            Route::post('do-register-agents', [UserController::class, 'registerAgent'])->name('registerAgent');
            Route::post('agent-inactive', [UserController::class, 'deActivateUser']);
            Route::get('agent-profile/{id}', [UserController::class, 'agentProfile'])->name('agentProfile');
            Route::get('agent-edit-profile/{id}', [UserController::class, 'agentEditProfile'])->name('agentEditProfile');
            Route::post('agent-update-profile', [UserController::class, 'agentUpdateProfile'])->name('agentUpdateProfile');
            Route::get('view-assigned-activities/{id}', [ActivityController::class, 'viewAssignedActivities'])->name('viewAssignedActivities');
            Route::get('assigned-task-details/{id}', [ActivityController::class, 'assignedTasksDetails'])->name('assignedTasksDetails');
            Route::get('view-monthly-progress/{agentId}', [UserActivityController::class, 'viewMonthlyProgress'])->name('viewMonthlyProgress');
            Route::get('user-discussion/{agentId}', [UserDiscussionController::class, 'discussionBoard'])->name('discussionBoard');
            Route::post('add-discussion', [UserDiscussionController::class, 'addDiscussion'])->name('addDiscussion');

            
            //manager
            Route::get('managers', [UserController::class, 'managers'])->name('managers.index');
            Route::get('recieved-request-details/{managerId}',  [UserController::class, 'recievedRequestDetails'])->name('recievedRequestDetails');
            Route::post('reject-managers-request',  [UserController::class, 'rejectManagersRequest'])->name('rejectManagersRequest');
            Route::get('rejected-manager-details/{managerId}',  [UserController::class, 'rejectedRequestDetails'])->name('rejectedRequestDetails');

             
            //activity management
            Route::get('activity-management', [ActivityController::class, 'index'])->name('activity.management.index');
            Route::post('assign-agent-activity', [ActivityController::class, 'assignAgentActivity'])->name('assignAgentActivity');
            Route::post('find-activity-by-id', [ActivityController::class, 'findActivitiesByCatId']);
            Route::get('create-activity-management', [ActivityController::class, 'createActivityManagement'])->name('createActivityManagement');   
            
            //category management
            Route::get('category-management', [CategoryController::class, 'index'])->name('category.index');
            
            //payment
            Route::get('payment-log', [ActualPaymentController::class, 'index'])->name('payment.index');
            Route::get('settings', [SettingController::class, 'index'])->name('settings.index');
            Route::get('feedback', [FeedbackController::class, 'index'])->name('feedback.index');


            //profile
            Route::get('my-profile', [UserController::class, 'myProfile'])->name('myProfile');
            Route::get('my-profile-edit', [UserController::class, 'myProfileEdit'])->name('myProfileEdit');
            Route::post('update-my-profile', [UserController::class, 'updateMyProfile'])->name('updateProfile');

            Route::post('password-change', [UserController::class, 'changePassword'])->name('passwordChange');
            Route::get('my-notifications', [NotificationController::class, 'getMyNotifications'])->name('myNotificaions');
            Route::get('feedback-detail-manager/{id}', [FeedbackController::class, 'feedbackDetailsManager'])->name('feedbackDetailsManager');
            Route::get('feedback-detail-agent/{id}', [FeedbackController::class, 'feedbackDetailsAgent'])->name('feedbackDetailsAgent');
            Route::get('delete-feedback/{id}', [FeedbackController::class, 'deleteFeedback'])->name('deleteFeedback');
            Route::get('edit-settings',  [SettingController::class, 'editSettings'])->name('editSettings');
            Route::post('update-settings',  [SettingController::class, 'updateSettings'])->name('updateSettings');
            Route::post('add-category', [CategoryController::class, 'addCategory'])->name('addCategory');
            Route::post('add-activity', [ActivityController::class, 'addActivity'])->name('addActivity');
            Route::post('update-category', [CategoryController::class, 'updateCategory'])->name('updateCategory');
            Route::post('update-activity', [ActivityController::class, 'updateActivity'])->name('updateActivity');
            Route::post('update-user-status', [UserController::class, 'updateUserStatus'])->name('updateUserStatus');
            Route::get('manager-profile/{id}', [UserController::class, 'managerProfile'])->name('managerProfile');
            Route::get('manager-edit-profile/{id}', [UserController::class, 'managerEditProfile'])->name('managerEditProfile');
            Route::post('manager-update-profile', [UserController::class, 'managerUpdateProfile'])->name('managerUpdateProfile');
            Route::get('managers-requests', [UserController::class, 'managersRequests'])->name('managersRequests');
            Route::get('inactive-managers', [UserController::class, 'inActiveManagers'])->name('inActiveManagers');
            Route::get('create-manager', [UserController::class, 'createManager'])->name('createManager');
            Route::post('add-manager', [UserController::class, 'addManager'])->name('addManager');
            Route::get('view-assigned-agents/{id}', [UserController::class, 'viewAssignedAgents'])->name('viewAssignedAgents');
            Route::get('get-activity-by-category/{id}', [ActivityController::class, 'getActivitiesByCatId'])->name('getActivitiesByCatId');

            Route::post('add-reminder', [ReminderNotificationController::class, 'addReminder'])->name('addReminder');

            
        });
    });    
});    

require __DIR__.'/auth.php';
